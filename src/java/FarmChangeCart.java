import entity.Farmingproducts;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class FarmChangeCart extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String unit = request.getParameter("farmunits");
        int units = Integer.parseInt(unit);
        System.out.println(units);
        HttpSession session = request.getSession(false);
        Farmingproducts prod = (Farmingproducts) session.getAttribute("farmeditable");
        HashMap<Farmingproducts, Integer> shoppingcart;
        shoppingcart = (HashMap<Farmingproducts, Integer>) session.getAttribute("farmcart");
        if(shoppingcart != null && shoppingcart.containsKey(prod)){
            shoppingcart.put(prod, units);
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Product has been updated in cart!!');");
                out.println("window.location.href = \"farmcheckout.jsp\";");
                out.println("</script>");
            }
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
