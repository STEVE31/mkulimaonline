import entity.Buyer;
import entity.Farmer;
import entity.Product;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class EachProd extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Product prod;
        
        String productid = request.getParameter("prodid");
        int prodid = Integer.parseInt(productid);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Farmer.class)
            .addAnnotatedClass(Product.class)
            .addAnnotatedClass(Buyer.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session sess = sessionFactory.openSession();
        String hql = "SELECT p FROM Product p WHERE p.id = :id";
        Query query = sess.createQuery(hql);
        query.setInteger("id", prodid);
        List<Product> products;
        List results = query.list();
        products = results;
        
        Iterator<Product> itr = products.iterator();
        while(itr.hasNext()){
            prod = itr.next();
            System.out.println(prod.getCategory());
            HttpSession session = request.getSession(false);
            session.setAttribute("buyprod", prod);
            session.setAttribute("vendor",prod.vendor.getUsername());
        }
        sess.close();
        HttpSession session = request.getSession(false);
        if(session.getAttribute("farmuseremail") != null){
            response.sendRedirect("farmeachproduct.jsp");
        }else if(session.getAttribute("useremail") != null){
            response.sendRedirect("eachprod.jsp");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
