import entity.Buyer;
import entity.Farmer;
import entity.Farmingproducts;
import entity.Orders;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class FarmUpdating extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Farmer user;
        String useremail = null;
        HttpSession session = request.getSession(false);
        if(session.getAttribute("farmuseremail") != null){
            SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;
            Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Farmer.class)
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Buyer.class)
                    .addAnnotatedClass(Farmingproducts.class)
                    .configure();
            serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            Session sess = sessionFactory.openSession();
            useremail = (String) session.getAttribute("farmuseremail");
            System.out.println(useremail);
            String hql = "FROM Farmer";
            Query query = sess.createQuery(hql);
            List <Farmer> farmers;
            List results = query.list();
            farmers = results;
            System.out.println(useremail);
            System.out.println(farmers);
            Iterator<Farmer> itr = farmers.iterator();
            while(itr.hasNext()){
                user = itr.next();
                if(user.getEmail().equals(useremail)){
                    System.out.println(useremail);
                    System.out.println(user);
                    String pickuplocation = request.getParameter("locale");
                    int total = (int) session.getAttribute("farmtotal");
                    Product product;
                    int prodtotal;
                    Farmingproducts farmproduct;
                    Date date = new Date();
                    HashMap<Farmingproducts, Integer> farmcart = null;
                    HashMap<Product, Integer> shoppingcart = null;
                    String orderid = request.getSession(false).getId();
                    if((session.getAttribute("cart") == null || session.getAttribute("cart") == null || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == "") && (session.getAttribute("farmcart") == null || session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || session.getAttribute("farmcart").toString() == "{}" || session.getAttribute("farmcart") == "")){
                        response.sendRedirect("farmlogin.jsp");
                    }else{
                        if(session.getAttribute("cart") == null || session.getAttribute("cart") == null || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == ""){
                        }else{
                            shoppingcart =  (HashMap<Product, Integer>) session.getAttribute("cart");
                            for(Map.Entry m:shoppingcart.entrySet()){
                                product = (Product) m.getKey();
                                int newer = (int) m.getValue();
                                prodtotal = newer * product.getPrice();
                                Orders order = new Orders();
                                order.setFarmer(user);
                                order.setDate(date);
                                order.setFarmproduct(product);
                                order.setLocation(pickuplocation);
                                order.setOrderid(orderid);
                                order.setPrice(product.getPrice());
                                order.setProducttotal(prodtotal);
                                order.setStatuspending(true);
                                order.setTotal(total);
                                order.setUnitsbought(newer);
                                System.out.println(order);
                                sess.beginTransaction();
                                sess.persist(order);
                                sess.getTransaction().commit();
                            }
                        }
                        if(session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals(null) || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || session.getAttribute("farmcart").toString() == "{}" || session.getAttribute("farmcart") == ""){
                        }else{
                            farmcart =  (HashMap<Farmingproducts, Integer>) session.getAttribute("farmcart");
                            for(Map.Entry g:farmcart.entrySet()){
                                farmproduct = (Farmingproducts) g.getKey(); 
                                int newer = (int) g.getValue();
                                prodtotal = newer * farmproduct.getPrice();
                                Orders order = new Orders();
                                order.setFarmer(user);
                                order.setDate(date);
                                order.setFarmingproduct(farmproduct);
                                order.setLocation(pickuplocation);
                                order.setOrderid(orderid);
                                order.setPrice(farmproduct.getPrice());
                                order.setProducttotal(prodtotal);
                                order.setStatuspending(true);
                                order.setTotal(total);
                                order.setUnitsbought(newer);
                                order.setFarmer(user);
                                System.out.println(order);
                                sess.beginTransaction();
                                sess.save(order);
                                sess.getTransaction().commit();
                            }
                        }
                    }
                }
            }
            sess.close();
        }else{
            response.sendRedirect("farmlogin.jsp");
        }
        
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

