import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogOut extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if(session.getAttribute("useremail")!= null){
            session.setAttribute("useremail", null);
            try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('You have logged out!!');");
                    out.println("window.location.href = \"login.jsp\";");
                    out.println("</script>");
            }
        }else if(session.getAttribute("farmuseremail") != null){
            session.setAttribute("farmuseremail", null);
            try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('You have logged out!!');");
                    out.println("window.location.href = \"farmlogin.jsp\";");
                    out.println("</script>");
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
