import entity.Buyer;
import entity.Farmer;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Login extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Buyer.class)
            .addAnnotatedClass(Farmer.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        Session sess = sessionFactory.openSession();
        String hql = "FROM Buyer";
        Query queryy = sess.createQuery(hql);
        List<Buyer> buying;
        List results = queryy.list();
        
        boolean logsuccess = false;
        boolean useravail = false;
        buying = results;
        Iterator<Buyer> itr = buying.iterator();
        while(itr.hasNext()){
            Buyer g = itr.next();
            if((email.equals(g.getEmail())) && (password.equals(g.getPassword()))){
                logsuccess = true;
                System.out.println(email + g.getEmail());
                System.out.println(password + g.getPassword());
            }
            if(email.equals(g.getEmail())){
                useravail = true;
            }
        }
        if(useravail == false){
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location = \"login.jsp\";");
                out.println("alert('User is not available!!');");
                out.println("</script>");
            }
        }else if((logsuccess == true) && (useravail == true)){
            HttpSession session = request.getSession();
            session.setAttribute("useremail", email);
            HashMap<Product, Integer> shoppingcart;
            shoppingcart =  (HashMap<Product, Integer>) session.getAttribute("cart");
            if (shoppingcart == null)
            {
                shoppingcart = new HashMap<>();
                session.setAttribute("cart", shoppingcart);
            }
            response.sendRedirect("shop.jsp");
        }
        else{
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location.href = \"login.jsp\";");
                out.println("alert('Incorrect login details!!');");
                out.println("</script>");
            }
        }
    }
}
