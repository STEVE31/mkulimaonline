import entity.Buyer;
import entity.Farmer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class farmLogin extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;
        
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Farmer.class)
            .addAnnotatedClass(Buyer.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        Session sess = sessionFactory.openSession();
 
        String hql = "FROM Farmer";
        Query queryy = sess.createQuery(hql);
        List<Farmer> farming;
        List results = queryy.list();
        
        boolean logsuccess = false;
        boolean useravail = false;
        farming = results;
        Iterator<Farmer> itr = farming.iterator();
        
        while(itr.hasNext()){
            Farmer g = itr.next();
            if((email.equals(g.getEmail())) && (password.equals(g.getPassword()))){
                logsuccess = true;
                System.out.println(email + g.getEmail());
                System.out.println(password + g.getPassword());
            }
            if(email.equals(g.getEmail())){
                useravail = true;
            }
        }
        if(useravail == false){
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location = \"farmlogin.jsp\";");
                out.println("alert('User is not available!!');");
                out.println("</script>");
            }
        }else if((logsuccess == true) && (useravail == true)){
            HttpSession session = request.getSession();
            session.setAttribute("farmuseremail", email);
            response.sendRedirect("postprod.jsp");
        }
        else{
            try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("self.location.href = \"farmlogin.jsp\";");
                out.println("alert('Incorrect login details!!');");
                out.println("</script>");
            }
        }
    }
}
