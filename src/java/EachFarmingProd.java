import entity.Buyer;
import entity.Farmer;
import entity.Farmingproducts;
import entity.Product;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Tariana
 */
public class EachFarmingProd extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Farmingproducts farmproduct;
        
        String farmproductid = request.getParameter("prodid");
        int farmprodid = Integer.parseInt(farmproductid);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Farmingproducts.class)
            .addAnnotatedClass(Farmer.class)
            .addAnnotatedClass(Product.class)
            .addAnnotatedClass(Buyer.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session sess = sessionFactory.openSession();
        String hql = "SELECT f FROM Farmingproducts f WHERE f.id = :id";
        Query query = sess.createQuery(hql);
        query.setInteger("id", farmprodid);
        List<Farmingproducts> farmproducts;
        List results = query.list();
        farmproducts = results;
        
        Iterator<Farmingproducts> itr = farmproducts.iterator();
        while(itr.hasNext()){
            farmproduct = itr.next();
            HttpSession session = request.getSession(false);
            session.setAttribute("farmbuyprod", farmproduct);
            System.out.println(farmproduct.getName());
        }
        sess.close();
        response.sendRedirect("farmingeachprod.jsp");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
