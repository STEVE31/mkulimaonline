import entity.Farmingproducts;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class FarmOrder extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pickuplocation = request.getParameter("locale");
        String useremail = null;
        HttpSession session = request.getSession(false);
        if(session.getAttribute("farmuseremail") != null){
            useremail = (String) session.getAttribute("farmuseremail");
        }else{
            response.sendRedirect("farmlogin.jsp");
        }
        Product product;
        int prodtotal;
        Farmingproducts farmproduct;
        HashMap<Farmingproducts, Integer> farmcart = null;
        HashMap<Product, Integer> shoppingcart = null;
        if((session.getAttribute("cart") == null || session.getAttribute("cart").equals(null) || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == "") && (session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals(null) || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || session.getAttribute("farmcart").toString() == "{}" || session.getAttribute("farmcart") == "")){
            response.sendRedirect("farmlogin.jsp");
        }else{
            if(session.getAttribute("cart") == null || session.getAttribute("cart").equals(null) || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == ""){
            }else{
                shoppingcart =  (HashMap<Product, Integer>) session.getAttribute("cart");
            }
            if(session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals(null) || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || session.getAttribute("farmcart").toString() == "{}" || session.getAttribute("farmcart") == ""){
            }else{
                farmcart =  (HashMap<Farmingproducts, Integer>) session.getAttribute("farmcart");
            }
        }
        int total = (int) session.getAttribute("farmtotal");
        int totals = total;
        String totalss = new Integer(totals).toString();
        String host = "smtp-mail.outlook.com";
        String user = "njorogesteve30@outlook.com";
        String password = "steniverse30";
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp" );
        props.put("mail.smtp.starttls.enable","true" );
        props.put("mail.smtp.host",host);  
        props.put("mail.smtp.auth", "true");
        Session sessionn = Session.getDefaultInstance(props,  
            new javax.mail.Authenticator() {  
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {  
                    return new PasswordAuthentication(user,password);  
                }  
        });
        MimeMessage message;  
        try {
            message = new MimeMessage(sessionn);
            message.setFrom(new InternetAddress(user));  
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(useremail));
            message.setSubject("Mkulima Online");
            String msgText1 = "Your order in mkulima online has been placed. \r\n";
            String msgText2 = "Here is the list of items purchased as a farmer \r\n";
            Multipart mp = new MimeMultipart();
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setText(msgText1 + "Pick Up your products within 5 working days \r\n" + msgText2 + "\r\n" + "The pickup location is " + pickuplocation + "\r\n" + "The total price for all the products is: " + totalss);
            mp.addBodyPart(mbp1);
            if(session.getAttribute("cart") == null || session.getAttribute("cart") == null || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || "{}".equals(session.getAttribute("cart").toString()) || session.getAttribute("cart") == ""){
            }else{
                for(Map.Entry m:shoppingcart.entrySet()){
                    product = (Product) m.getKey();
                    int newer = (int) m.getValue();
                    System.out.println(newer);
                    System.out.println(m.getValue());
                    prodtotal = newer * product.getPrice();
                    String msgText3 = ("Farm Product \r\n");
                    String msgtxt4 = msgText3.concat("Product name: " + product.getName() + "\r\n" + "Category: " + product.getCategory() + "\r\n" + "Type: " + product.getType() + "\r\n" + "Price per unit: " + product.getPrice() + "\r\n" + "Units bought:" + m.getValue().toString() + "\r\n" + "Product total: "  + prodtotal);
                    MimeBodyPart mbp2 = new MimeBodyPart();
                    mbp2.setText(msgtxt4);
                    mp.addBodyPart(mbp2);
                }
            }
            if(session.getAttribute("farmcart") == null || session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || "{}".equals(session.getAttribute("farmcart").toString()) || session.getAttribute("farmcart") == ""){
            }else{
                for(Map.Entry g:farmcart.entrySet()){
                    farmproduct = (Farmingproducts) g.getKey(); 
                    int newer = (int) g.getValue();
                    System.out.println(newer);
                    System.out.println(g.getValue());
                    prodtotal = newer * farmproduct.getPrice();
                    String msgText3 = ("Farming Product \r\n");
                    String msgtxt4 = msgText3.concat("Product name: " +  farmproduct.getName() + "\r\n" +  "Category: " +  farmproduct.getCategory() + "\r\n" +  "Vendor: " +  farmproduct.getVendor() + "\r\n" + "Price per unit: " + farmproduct.getPrice() + "\r\n" +  "Units bought: " + g.getValue().toString() + "\r\n" + "Product total: "  + prodtotal);
                    MimeBodyPart mbp3 = new MimeBodyPart();
                    mbp3.setText(msgtxt4);
                    mp.addBodyPart(mbp3);
                }
            }
            message.setContent(mp);
            Transport.send(message);
            System.out.println("farmer message sending was successful...");
            response.sendRedirect("Deletion");
        } catch (MessagingException ex) {
            Logger.getLogger(FarmOrder.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
