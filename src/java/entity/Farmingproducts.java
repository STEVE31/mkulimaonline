package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tariana
 */
@Entity
@Table(name = "farmingproducts")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Farmingproducts.findAll", query = "SELECT f FROM Farmingproducts f")
    , @NamedQuery(name = "Farmingproducts.findById", query = "SELECT f FROM Farmingproducts f WHERE f.id = :id")
    , @NamedQuery(name = "Farmingproducts.findByName", query = "SELECT f FROM Farmingproducts f WHERE f.name = :name")
    , @NamedQuery(name = "Farmingproducts.findByCategory", query = "SELECT f FROM Farmingproducts f WHERE f.category = :category")
    , @NamedQuery(name = "Farmingproducts.findByVendor", query = "SELECT f FROM Farmingproducts f WHERE f.vendor = :vendor")
    , @NamedQuery(name = "Farmingproducts.findByPrice", query = "SELECT f FROM Farmingproducts f WHERE f.price = :price")
    , @NamedQuery(name = "Farmingproducts.findByUnits", query = "SELECT f FROM Farmingproducts f WHERE f.units = :units")
    , @NamedQuery(name = "Farmingproducts.findByDescription", query = "SELECT f FROM Farmingproducts f WHERE f.description = :description")})
public class Farmingproducts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Lob
    @Column(name = "image")
    private byte[] image;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "category")
    private String category;
    @Basic(optional = false)
    @Column(name = "vendor")
    private String vendor;
    @Basic(optional = false)
    @Column(name = "price")
    private int price;
    @Basic(optional = false)
    @Column(name = "units")
    private int units;
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "farmingproduct")
    private List<Orders> orders;

    public Farmingproducts() {
    }

    public Farmingproducts(Integer id) {
        this.id = id;
    }

    public Farmingproducts(Integer id, byte[] image, String name, String category, String vendor, int price, int units) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.category = category;
        this.vendor = vendor;
        this.price = price;
        this.units = units;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Farmingproducts)) {
            return false;
        }
        Farmingproducts other = (Farmingproducts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Farmingproducts[ id=" + id + " ]";
    }
    
}
