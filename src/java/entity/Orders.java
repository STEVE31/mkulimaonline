/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tariana
 */
@Entity
@Table(name = "orders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Orders.findAll", query = "SELECT o FROM Orders o")
    , @NamedQuery(name = "Orders.findById", query = "SELECT o FROM Orders o WHERE o.id = :id")
    , @NamedQuery(name = "Orders.findByOrderid", query = "SELECT o FROM Orders o WHERE o.orderid = :orderid")
    , @NamedQuery(name = "Orders.findByFarmer", query = "SELECT o FROM Orders o WHERE o.farmer = :farmer")
    , @NamedQuery(name = "Orders.findByBuyer", query = "SELECT o FROM Orders o WHERE o.buyer = :buyer")
    , @NamedQuery(name = "Orders.findByLocation", query = "SELECT o FROM Orders o WHERE o.location = :location")
    , @NamedQuery(name = "Orders.findByDate", query = "SELECT o FROM Orders o WHERE o.date = :date")
    , @NamedQuery(name = "Orders.findByStatuspending", query = "SELECT o FROM Orders o WHERE o.statuspending = :statuspending")
    , @NamedQuery(name = "Orders.findByFarmingproduct", query = "SELECT o FROM Orders o WHERE o.farmingproduct = :farmingproduct")
    , @NamedQuery(name = "Orders.findByFarmproduct", query = "SELECT o FROM Orders o WHERE o.farmproduct = :farmproduct")
    , @NamedQuery(name = "Orders.findByUnitsbought", query = "SELECT o FROM Orders o WHERE o.unitsbought = :unitsbought")
    , @NamedQuery(name = "Orders.findByProducttotal", query = "SELECT o FROM Orders o WHERE o.producttotal = :producttotal")
    , @NamedQuery(name = "Orders.findByTotal", query = "SELECT o FROM Orders o WHERE o.total = :total")
    , @NamedQuery(name = "Orders.findByPrice", query = "SELECT o FROM Orders o WHERE o.price = :price")})
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "orderid")
    private String orderid;
    @Column(name = "farmer")
    @ManyToOne(fetch=FetchType.EAGER)
    private Farmer farmer;
    @Column(name = "buyer")
    @ManyToOne(fetch=FetchType.EAGER)
    private Buyer buyer;
    @Basic(optional = false)
    @Column(name = "location")
    private String location;
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @Column(name = "statuspending")
    private boolean statuspending;
    @Column(name = "farmingproduct")
    @ManyToOne(fetch=FetchType.EAGER)
    private Farmingproducts farmingproduct;
    @Column(name = "farmproduct")
    @ManyToOne
    private Product farmproduct;
    @Basic(optional = false)
    @Column(name = "unitsbought")
    private int unitsbought;
    @Basic(optional = false)
    @Column(name = "producttotal")
    private int producttotal;
    @Basic(optional = false)
    @Column(name = "total")
    private int total;
    @Basic(optional = false)
    @Column(name = "price")
    private int price;

    public Orders() {
    }

    public Orders(Integer id) {
        this.id = id;
    }

    public Orders(Integer id, String orderid, String location, Date date, boolean statuspending, int unitsbought, int producttotal, int total, int price) {
        this.id = id;
        this.orderid = orderid;
        this.location = location;
        this.date = date;
        this.statuspending = statuspending;
        this.unitsbought = unitsbought;
        this.producttotal = producttotal;
        this.total = total;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean getStatuspending() {
        return statuspending;
    }

    public void setStatuspending(boolean statuspending) {
        this.statuspending = statuspending;
    }

    public Farmingproducts getFarmingproduct() {
        return farmingproduct;
    }

    public void setFarmingproduct(Farmingproducts farmingproduct) {
        this.farmingproduct = farmingproduct;
    }

    public Product getFarmproduct() {
        return farmproduct;
    }

    public void setFarmproduct(Product farmproduct) {
        this.farmproduct = farmproduct;
    }

    public int getUnitsbought() {
        return unitsbought;
    }

    public void setUnitsbought(int unitsbought) {
        this.unitsbought = unitsbought;
    }

    public int getProducttotal() {
        return producttotal;
    }

    public void setProducttotal(int producttotal) {
        this.producttotal = producttotal;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Orders)) {
            return false;
        }
        Orders other = (Orders) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Orders[ id=" + id + " ]";
    }
    
}
