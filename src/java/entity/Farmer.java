/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tariana
 */
@Entity
@Table(name = "farmer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Farmer.findAll", query = "SELECT f FROM Farmer f")
    , @NamedQuery(name = "Farmer.findById", query = "SELECT f FROM Farmer f WHERE f.id = :id")
    , @NamedQuery(name = "Farmer.findByUsername", query = "SELECT f FROM Farmer f WHERE f.username = :username")
    , @NamedQuery(name = "Farmer.findByFname", query = "SELECT f FROM Farmer f WHERE f.fname = :fname")
    , @NamedQuery(name = "Farmer.findByLname", query = "SELECT f FROM Farmer f WHERE f.lname = :lname")
    , @NamedQuery(name = "Farmer.findByEmail", query = "SELECT f FROM Farmer f WHERE f.email = :email")
    , @NamedQuery(name = "Farmer.findByPhoneno", query = "SELECT f FROM Farmer f WHERE f.phoneno = :phoneno")
    , @NamedQuery(name = "Farmer.findByPassword", query = "SELECT f FROM Farmer f WHERE f.password = :password")
    , @NamedQuery(name = "Farmer.findByLocation", query = "SELECT f FROM Farmer f WHERE f.location = :location")})
public class Farmer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    public Integer id;
    @Basic(optional = false)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @Column(name = "fname")
    private String fname;
    @Basic(optional = false)
    @Column(name = "lname")
    private String lname;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "phoneno")
    private int phoneno;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @Column(name = "location")
    private String location;
    @OneToMany(mappedBy = "vendor", cascade= CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
    public List<Product> products;
    @OneToMany(mappedBy = "farmer", cascade= CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Orders> orders;
    
    public Farmer() {
    }

    public Farmer(Integer id) {
        this.id = id;
    }

    public Farmer(Integer id, String username, String fname, String lname, String email, int phoneno, String password, String location) {
        this.id = id;
        this.username = username;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.phoneno = phoneno;
        this.password = password;
        this.location = location;
    }

    public Integer getId() {
        return id;
    }
    
    //**public void addProduct(Product product) {
     //   this.products.add(product);   
    //}

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(int phoneno) {
        this.phoneno = phoneno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Farmer)) {
            return false;
        }
        Farmer other = (Farmer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Farmer[ id=" + id + " ]";
    }
    
}
