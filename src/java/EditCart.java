import entity.Buyer;
import entity.Farmer;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class EditCart extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Product prod;
        String productid = request.getParameter("id");
        int prodid = Integer.parseInt(productid);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Buyer.class)
            .addAnnotatedClass(Farmer.class)
            .addAnnotatedClass(Product.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session sess = sessionFactory.openSession();
        String hql = "SELECT p FROM Product p WHERE p.id = :id";
        Query query = sess.createQuery(hql);
        query.setInteger("id", prodid);
        List<Product> products;
        List results = query.list();
        products = results;
        Iterator<Product> itr = products.iterator();
        HttpSession session = request.getSession(false);
        while(itr.hasNext()){
            prod = itr.next();
            System.out.println(prod);
            session.setAttribute("editable", prod);
            session.setAttribute("editvendor",prod.vendor.getUsername());
            if(session.getAttribute("useremail") != null){
                response.sendRedirect("editcart.jsp");
            }else if(session.getAttribute("farmuseremail") != null){
                response.sendRedirect("farmeditcart.jsp");
            }
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
