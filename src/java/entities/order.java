package entities;

import entity.Buyer;
import java.util.*;


public class order {
    int orderid;
    Buyer buyer;
    List<product> products;
    int total;
    Date orderdate;
    
    public order(){
    }
    
    public int getorderid(){
        return orderid;
    }
    public int gettotal(){
        return total;
    }
    public Date getorderdate(){
        return orderdate;
    }
    public Buyer getbuyer(){
        return buyer;
    }
    public List<product> getproducts(){
        return products;
    }
    public void setorderid(int orderid){
        this.orderid = orderid;
    }
    public void setorderdate(Date orderdate){
        this.orderdate = orderdate;
    }
    public void settotal(int total){
        this.total = total;
    }
    public void setbuyer(Buyer buyer){
        this.buyer = buyer;
    }
    public void setproducts(List<product> products){
        this.products = products;
    }
}
