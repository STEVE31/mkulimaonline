package entities;

import entity.Farmer;

public class product {
    int id;
    String name;
    String type;
    String category;
    int price;
    int units;
    String description;
    Farmer vendor;
    
    public product(){
    }
    
    public String getname()
    {
        return name;
    }
    public String getcategory()
    {
        return category;
    }
    public int getprice()
    {
        return price;
    }
    public String gettype()
    {
        return type;
    }
    public int getunits()
    {
        return units;
    }
    public int getid()
    {
        return id;
    }
    public Farmer getvendor()
    {
        return vendor;
    }
    public String getdescription()
    {
        return description;
    }
    public void setname(String name) 
    {
        this.name = name;
    }
    public void setdescription(String description) 
    {
        this.description = description;
    }
    public void setcategory(String category) 
    {
        this.category = category;
    }
    public void settype(String type) 
    {
        this.type = type;
    }
    public void setprice(int price) 
    {
        this.price = price;
    }
    public void setid(int id)
    {
        this.id = id;
    }
    public void setunits(int units) 
    {
        this.units = units;
    }
    public void setvendor(Farmer vendor) 
    {
        this.vendor = vendor;
    }
}
