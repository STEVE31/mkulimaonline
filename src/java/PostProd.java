import entity.*;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class PostProd extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Product prod = new Product();
        Farmer vendor;
        int units;
        int price;
        
        String name = request.getParameter("name");
        String category = request.getParameter("category");
        String type = request.getParameter("type");
        String pri = request.getParameter("price");
        price = Integer.parseInt(pri);
        String unit = request.getParameter("units");
        units = Integer.parseInt(unit);
        String description = request.getParameter("description");
        
        prod.setName(name);
        prod.setCategory(category);
        prod.setDescription(description);
        prod.setPrice(price);
        prod.setType(type);
        prod.setUnits(units);
        
        HttpSession session = request.getSession(false);
        String usermail = (String) session.getAttribute("farmuseremail");
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Farmer.class)
            .addAnnotatedClass(Product.class)
            .addAnnotatedClass(Buyer.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session sess = sessionFactory.openSession();
        
        String hql = "SELECT f FROM Farmer f WHERE f.email = :email";
        Query query = sess.createQuery(hql);
        query.setString("email", usermail);
        System.out.println(usermail);
        List<Farmer> user;
        List results = query.list();
        user = results;
        
        Iterator<Farmer> itr = user.iterator();
        while(itr.hasNext()){
            vendor = itr.next();
            prod.setVendor(vendor);
            System.out.println(vendor.getId());
            System.out.println(vendor.getUsername());
            System.out.println(vendor.getEmail());
            System.out.println(prod.getUnits());
            sess.beginTransaction();
            sess.persist(prod);
            sess.getTransaction().commit();
            sess.close();
        }
        response.sendRedirect("myproducts.jsp");  
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
