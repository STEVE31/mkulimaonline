import entity.Buyer;
import entity.Farmer;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Register extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {   
        
        Buyer buyerreg = new Buyer();
        
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String username = request.getParameter("username");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String phone = request.getParameter("phoneno");
        int phoneno = Integer.parseInt(phone);
        String location = request.getParameter("location");
        
        buyerreg.setEmail(email);
        buyerreg.setPassword(password);
        buyerreg.setFname(fname);
        buyerreg.setLname(lname);
        buyerreg.setUsername(username);
        buyerreg.setPhoneno(phoneno);
        buyerreg.setLocation(location);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Buyer.class)
            .addAnnotatedClass(Farmer.class)
            .addResource("entity/Buyer.hbm.xml")
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        Session sess = sessionFactory.openSession();
        String hql = "FROM Buyer";
        Query queryy = sess.createQuery(hql);
        List<Buyer> buying;
        List results = queryy.list();
        
        buying = results;
        Iterator<Buyer> itr = buying.iterator();    
        while(itr.hasNext()){
            Buyer g = itr.next();
            System.out.println(buyerreg.getEmail() + g.getEmail());
            System.out.println(buyerreg.getPhoneno() + g.getPhoneno());
            System.out.println(buyerreg.getUsername() + g.getUsername());
            if(buyerreg.getEmail().equalsIgnoreCase(g.getEmail())){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("window.location.href = \"register.jsp\";");
                    out.println("alert('Email already exists!!');");
                    out.println("</script>");
                }
            }
            else if(buyerreg.getUsername().equalsIgnoreCase(g.getUsername())){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("window.location.href = \"register.jsp\";");
                    out.println("alert('Username already exists!!');");
                    out.println("</script>");
                }
            }
            else if(buyerreg.getPhoneno() == g.getPhoneno()){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("window.location.href = \"register.jsp\";");
                    out.println("alert('Phone number already exists!!');");
                    out.println("</script>");
                }
            }
            else {
                System.out.println("No available username, password and email");
                
            }
        }
        sess.beginTransaction();
        sess.persist(buyerreg);
        sess.getTransaction().commit();
        HttpSession session = request.getSession();
        session.setAttribute("useremail", email);
        HashMap<Product, Integer> shoppingcart;
        shoppingcart =  (HashMap<Product, Integer>) session.getAttribute("cart");
        if (shoppingcart == null)
        {
            shoppingcart = new HashMap<>();
            session.setAttribute("cart", shoppingcart);
        }
        System.out.println("success"); 
        response.sendRedirect("shop.jsp");
        sess.close(); 
    }
    
    @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response)
               throws IOException, ServletException {
      doGet(request, response);
   }
}
