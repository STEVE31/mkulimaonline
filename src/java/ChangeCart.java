import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangeCart extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String unit = request.getParameter("units");
        int units = Integer.parseInt(unit);
        HttpSession session = request.getSession(false);
        Product prod = (Product) session.getAttribute("editable");
        HashMap<Product, Integer> shoppingcart;
        shoppingcart =  (HashMap<Product, Integer>) session.getAttribute("cart");
        if(shoppingcart != null && shoppingcart.containsKey(prod)){
            shoppingcart.put(prod, units);
            if(session.getAttribute("useremail") != null){
                try (PrintWriter out = response.getWriter()) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Product has been updated in cart!!');");
                        out.println("window.location.href = \"checkout.jsp\";");
                        out.println("</script>");
                }
            }else if(session.getAttribute("farmuseremail") != null){
                try (PrintWriter out = response.getWriter()) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Product has been updated in cart!!');");
                        out.println("window.location.href = \"farmcheckout.jsp\";");
                        out.println("</script>");
                }
            }
        }
        
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
