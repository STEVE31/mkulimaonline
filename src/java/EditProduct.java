import entity.Buyer;
import entity.Farmer;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class EditProduct extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int units;
        int price;
        
        String name = request.getParameter("name");
        String category = request.getParameter("category");
        String type = request.getParameter("type");
        String pri = request.getParameter("price");
        price = Integer.parseInt(pri);
        String unit = request.getParameter("units");
        units = Integer.parseInt(unit);
        String description = request.getParameter("description");
        
        HttpSession session = request.getSession(false);
        Product currentprod = (Product) session.getAttribute("editprod");
        currentprod.setName(name);
        currentprod.setCategory(category);
        currentprod.setDescription(description);
        currentprod.setPrice(price);
        currentprod.setType(type);
        currentprod.setUnits(units);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Farmer.class)
            .addAnnotatedClass(Product.class)
            .addAnnotatedClass(Buyer.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session sess = sessionFactory.openSession();
            sess.beginTransaction();
            if (sess.isOpen()){
                sess.update(currentprod);
                sess.getTransaction().commit();
                sess.close();
            }
            try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Product has been updated!!');");
                    out.println("window.location.href = \"myproducts.jsp\";");
                    out.println("</script>");
            }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
