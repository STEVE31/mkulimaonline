import entity.Buyer;
import entity.Farmer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class farmRegister extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Farmer farmerreg = new Farmer();
        
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String username = request.getParameter("username");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String phone = request.getParameter("phoneno");
        int phoneno = Integer.parseInt(phone);
        String location = request.getParameter("location");
        
        farmerreg.setEmail(email);
        farmerreg.setPassword(password);
        farmerreg.setFname(fname);
        farmerreg.setLname(lname);
        farmerreg.setUsername(username);
        farmerreg.setPhoneno(phoneno);
        farmerreg.setLocation(location);
        
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Buyer.class)
            .addAnnotatedClass(Farmer.class)
            .addResource("entity/Farmer.hbm.xml")
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        Session sess = sessionFactory.openSession();
 
        String hql = "FROM Farmer";
        Query queryy = sess.createQuery(hql);
        List<Farmer> farming;
        List results = queryy.list();
        
        farming = results;
        Iterator<Farmer> itr = farming.iterator();
        
        while(itr.hasNext()){
            Farmer g = itr.next();
            System.out.println(farmerreg.getEmail() + g.getEmail());
            if(farmerreg.getEmail().equalsIgnoreCase(g.getEmail())){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Email already exists!!');");
                    out.println("window.location.href = \"register.jsp\";");
                    out.println("</script>");
                }
            }
            else if(farmerreg.getUsername().equalsIgnoreCase(g.getUsername())){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Username already exists!!');");
                    out.println("window.location.href = \"register.jsp\";");
                    out.println("</script>");
                }
            }
            else if(farmerreg.getPhoneno() == g.getPhoneno()){
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Phone number already exists!!');");
                    out.println("window.location.href = \"register.jsp\";");
                    out.println("</script>");
                }
            }
            else {
                System.out.println("No available username, password and email");
            }
        }
        sess.beginTransaction();
        sess.persist(farmerreg);
        sess.getTransaction().commit();
        HttpSession session = request.getSession();
        session.setAttribute("farmuseremail", email);
        System.out.println("success"); 
        response.sendRedirect("postprod.jsp");
        sess.close();
    }
}
