import entity.Buyer;
import entity.Farmer;
import entity.Product;
import java.io.IOException;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class EditProd extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Product prod;
        
        String productid = request.getParameter("prodid");
        int prodid = Integer.parseInt(productid);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Farmer.class)
            .addAnnotatedClass(Product.class)
            .addAnnotatedClass(Buyer.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session sess = sessionFactory.openSession();
        String hql = "SELECT p FROM Product p WHERE p.id = :id";
        Query query = sess.createQuery(hql);
        query.setInteger("id", prodid);
        List<Product> products;
        List results = query.list();
        products = results;
        HttpSession session = request.getSession(false);
        
        Iterator<Product> itr = products.iterator();
        while(itr.hasNext()){
            prod = itr.next();
            session.setAttribute("editprod", prod); 
        }
        sess.close();
        response.sendRedirect("farmeachprod.jsp");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
