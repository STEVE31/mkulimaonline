
import entities.product;
import entity.Buyer;
import entity.Farmer;
import entity.Farmingproducts;
import entity.Orders;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Deletion extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if(session.getAttribute("farmuseremail") != null){
            SessionFactory sessionFactory;
            ServiceRegistry serviceRegistry;
            Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Farmer.class)
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Buyer.class)
                    .addAnnotatedClass(Farmingproducts.class)
                    .configure();
            serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            Session sess = sessionFactory.openSession();
            if((session.getAttribute("cart") == null || session.getAttribute("cart") == null || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == "") && (session.getAttribute("farmcart") == null || session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || session.getAttribute("farmcart").toString() == "{}" || session.getAttribute("farmcart") == "")){
                        response.sendRedirect("farmlogin.jsp");
            }else{
                if(session.getAttribute("cart") == null || session.getAttribute("cart") == null || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == ""){
                }else{
                    HashMap<Product, Integer> shoppingcart = (HashMap<Product, Integer>) session.getAttribute("cart");
                    for(Map.Entry m:shoppingcart.entrySet()){
                        Product product = (Product) m.getKey();
                        int newer = (int) m.getValue();
                        String hql = "SELECT p FROM Product p WHERE p.id = :id";
                        Query query = sess.createQuery(hql);
                        query.setInteger("id", product.getId());
                        List<Product> products;
                        List results = query.list();
                        products = results;
                        Iterator<Product> itr = products.iterator();
                        while(itr.hasNext()){
                            Product prod = itr.next();
                            int units = prod.getUnits();
                            units -= newer;
                            if(units == 0){
                                sess.beginTransaction();
                                prod.setVendor(null);
                                sess.delete(prod);
                                sess.getTransaction().commit();
                            }else{
                                prod.setUnits(units);
                                sess.beginTransaction();
                                sess.update(prod);
                                sess.getTransaction().commit();
                            }
                        }
                    }
                }
                if(session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals(null) || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || session.getAttribute("farmcart").toString() == "{}" || session.getAttribute("farmcart") == ""){
                }else{
                HashMap<Farmingproducts, Integer> farmcart = (HashMap<Farmingproducts, Integer>) session.getAttribute("farmcart");
                        for(Map.Entry g:farmcart.entrySet()){
                            Farmingproducts farmproduct = (Farmingproducts) g.getKey(); 
                            int newer = (int) g.getValue();
                            String hql = "SELECT f FROM Farmingproducts f WHERE f.id = :id";
                            Query query = sess.createQuery(hql);
                            query.setInteger("id", farmproduct.getId());
                            List<Farmingproducts> farmproducts;
                            List results = query.list();
                            farmproducts = results;

                            Iterator<Farmingproducts> itr = farmproducts.iterator();
                        while(itr.hasNext()){
                            farmproduct = itr.next();
                            int units = farmproduct.getUnits();
                            units -= newer;
                            if(units == 0){
                                sess.beginTransaction();
                                sess.delete(farmproduct);
                                sess.getTransaction().commit();
                            }else{
                                farmproduct.setUnits(units);
                                sess.beginTransaction();
                                sess.update(farmproduct);
                                sess.getTransaction().commit();
                            }
                        }
                    }
                }
                session.setAttribute("cart", null);
                session.setAttribute("farmcart", null);
                try (PrintWriter out = response.getWriter()) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Order placed. Please check your email!!');");
                    out.println("window.location.href = \"farmcheckout.jsp\";");
                    out.println("</script>");
                }
            } 
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
