import entity.Buyer;
import entity.Farmer;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class RemoveFromCart extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Product prod;
        String productid = request.getParameter("delid");
        System.out.println(productid);
        int prodid = Integer.parseInt(productid);
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Farmer.class)
            .addAnnotatedClass(Product.class)
            .addAnnotatedClass(Buyer.class)
            .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        
        Session sess = sessionFactory.openSession();
        String hql = "SELECT p FROM Product p WHERE p.id = :id";
        Query query = sess.createQuery(hql);
        query.setInteger("id", prodid);
        List<Product> products;
        List results = query.list();
        products = results;
        HttpSession session = request.getSession(false);
        HashMap<Product, Integer> shoppingcart;
        shoppingcart =  (HashMap<Product, Integer>) session.getAttribute("cart");
        Iterator<Product> itr = products.iterator();
        while(itr.hasNext()){
            prod = itr.next();
            if(shoppingcart != null && shoppingcart.containsKey(prod)){
                shoppingcart.remove(prod);
                if(session.getAttribute("useremail") != null){
                    try (PrintWriter out = response.getWriter()) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Product has been removed from cart!!');");
                            out.println("window.location.href = \"checkout.jsp\";");
                            out.println("</script>");
                    }
                }else if(session.getAttribute("farmuseremail") != null){
                    try (PrintWriter out = response.getWriter()) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Product has been removed from cart!!');");
                            out.println("window.location.href = \"farmcheckout.jsp\";");
                            out.println("</script>");
                    }
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
