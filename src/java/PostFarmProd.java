import entity.Buyer;
import entity.Farmer;
import entity.Farmingproducts;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class PostFarmProd extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Farmingproducts product = new Farmingproducts();
        int units;
        int price;
        
        String name = request.getParameter("name");
        String image = request.getParameter("image");
        String vendor = request.getParameter("vendor");
        String category = request.getParameter("category");
        String pri = request.getParameter("price");
        price = Integer.parseInt(pri);
        String unit = request.getParameter("units");
        units = Integer.parseInt(unit);
        String description = request.getParameter("description");
        
        File file = new File(image);
        
        byte[] bFile = new byte[(int) file.length()];
        
        try {
            //convert file into array of bytes
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                //convert file into array of bytes
                fileInputStream.read(bFile);
            }
        } catch (IOException e) {
	     e.printStackTrace();
        }
        
        product.setImage(bFile);
        product.setName(name);
        product.setCategory(category);
        product.setVendor(vendor);
        product.setPrice(price);
        product.setUnits(units);
        product.setDescription(description);
        
        SessionFactory sessionFactory;
        ServiceRegistry serviceRegistry;

        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Farmer.class)
                .addAnnotatedClass(Farmingproducts.class)
                .addAnnotatedClass(Buyer.class)
                .configure();
        
        serviceRegistry = new ServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .configure("hibernate.cfg.xml")
                .build();        
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        Session sess = sessionFactory.openSession();
        sess.beginTransaction();
        sess.persist(product);
        sess.getTransaction().commit();
        sess.close();
        try (PrintWriter out = response.getWriter()) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Farming Product has been updated!!');");
                out.println("window.location.href = \"myupdates.jsp\";");
                out.println("</script>");
            }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
