<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- footer that spans over all the pages -->
<br>
<div class="feet">
    <p>&copy; Copyright 2058, Mkulima Online</p>
    <p>Contact Us On:  
        <a href="#" class="fa fa-facebook"></a>Facebook
        <a href="#" class="fa fa-twitter"></a>Twitter
        <a href="#" class="fa fa-google"></a>Google
        <a href="#" class="fa fa-instagram"></a>Instagram
        <span class="glyphicon glyphicon-envelope"></span><a href="tel:+254-725-603-808">+254725603808</a>
    </p>
</div>
<style>
    .feet {
      position: fixed;
      left: 0;
      bottom: 0;
      width: 100%;
      background-color: gainsboro;
      color: appworkspace;
      text-align: center;
    }
    a {
    text-decoration: none;
    }
    a:link, a:visited {
        color: inherit;
    }
</style>