<%@page import="org.apache.tomcat.util.codec.binary.Base64"%>
<%@page import="entity.Farmingproducts"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //checks whether there is a session attribute farmbuyprod that exists and redirects to the loginpage if it does not
    if(session.getAttribute("farmbuyprod") == null){
        response.sendRedirect("login.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="farmloginheader.jsp"%>
        <title>Farming Each Product</title>
    </head>
    <body>
        <div class="container">
            <%
                //casts a session attribute farmbuyprod to an entiry Farmingproducts
                Farmingproducts currentprod = (Farmingproducts) session.getAttribute("farmbuyprod");
                //gets the image of the product in bytes
                byte[] bFile = currentprod.getImage();
                //encodes the image bytes into an encodebase64
                byte[] encodeBase64 = Base64.encodeBase64(bFile);
                //converts the encodebase64 to a string which helps display the image
                String base64DataString = new String(encodeBase64 , "UTF-8");
            %>
            <img src="data:image/png;base64, <%=base64DataString%>" height="300px" width="300px">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricing">
                    <h3 class="price">
                        Ksh.
                        <%=currentprod.getPrice()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Product Name:<br>
                        <%=currentprod.getName()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Units:
                        <%=currentprod.getUnits()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Category:
                        <%=currentprod.getCategory()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Vendor:
                        <%=currentprod.getVendor()%>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>
                    Description
                    </h3>
                    <textarea name="description" rows="5" class="txt" disabled><%= currentprod.getDescription()%></textarea>
                </div>
            </div>
            <!-- A Farming products entity can be added to a cart according to a maximum  -->
            <form action="AddFarmToCart">
                <input type="hidden" name="prodid" value="<%=currentprod.getId()%>">
                <button class="btn btn-primary mb-2 mybtn">Add to Cart</button>
            </form>
        </div>
        <style>
            .pricing{
                background-color: darkgray;
            }
            .price{
                text-align:right;
                color: bisque;
            }
            .name{
                text-align:right;
            }
            .container{
                padding-bottom: 100px;
            }
            .txt{
                border: solid #cccccc;
                width: 100%;
                color: #999999;
            }
            .btn{
                width: 100%;
            }
        </style>
        </div>
        <%@ include file="footer.jsp" %>
    </body>
</html>
