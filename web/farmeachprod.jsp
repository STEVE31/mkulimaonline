<%@page import="entity.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <%
        //checks wherher there is an attribute within the session with the name editprod
        if(session.getAttribute("editprod") == null){
            response.sendRedirect("farmlogin.jsp");
        }
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit product</title>
        <%@ include file="farmloginheader.jsp" %>
    </head>
    <body>
        <h1>Edit your product</h1>
        <div class="container">
            <%
                //casts the attribute editprod to an entity Product
                Product currentproduct = (Product) session.getAttribute("editprod");
                //changes the photo source according to the category
                if(currentproduct.getType().equals("Pineapples")){
            %>
                <img src="pineapple.jpg" height="300px" width="300px">
            <%
                }else if(currentproduct.getType().equals("Bananas")){
            %>
                <img src="banana.jpg" height="300px" width="300px">
            <%
                }else if(currentproduct.getType().equals("Avocado")){
            %>
                <img src="avocado.jpg" height="300px" width="300px">
            <%
                }
            %>
            <!-- A form that stores the new values of the product which have all been first preset to the previous values-->
            <!-- The values changed are then updated into the database when the edit button is clicked -->
            <form action="EditProduct">
                <label for="name" class="mr-sm-2">Product name:</label>
                <input name="name" type="text" class="form-control mb-2 mr-sm-2" id="name" value="<%= currentproduct.getName()%>" required>
                
                <label for="category">Select category:</label>
                <select name="category" class="form-control" id="category" required >
                    <option value="Fruit">Fruit</option>
                </select>
                
                <label for="type">Select type:</label>
                <select name="type" class="form-control" id="type" required>
                    <option value="Pineapples">Pineapple</option>
                    <option value="Bananas">Banana</option>
                    <option value="Avocado">Avocado</option>
                </select>
                <!-- Changes the options of the product type in order to preset it to the current product type-->
                <% if(currentproduct.getType().toString().equals("Pineapples")){
                    %>
                    <script>
                        document.getElementById('type').getElementsByTagName('option')[0].selected = true;
                    </script>
                <% }else if(currentproduct.getType().toString().equals("Bananas")){
                    %>
                    <script>
                        document.getElementById('type').getElementsByTagName('option')[1].selected = true;
                    </script>
                <% }else if(currentproduct.getType().toString().equals("Avocado")){
                    %>
                    <script>
                        document.getElementById('type').getElementsByTagName('option')[2].selected = true;
                    </script>
                <% }%>
                <br><br>
                <label for="price" class="mr-sm-2">Price:</label>
                <input name="price" type="number" class="form-control mb-2 mr-sm-2" id="price" min="1" value="<%= currentproduct.getPrice()%>" required>
                <label for="units" class="mr-sm-2">Units:</label>
                <input name="units" type="number" class="form-control mb-2 mr-sm-2" id="units" min="1" value="<%= currentproduct.getUnits()%>" required>
                <br><br>
                <label for="description">Description:</label>
                <textarea name="description" class="form-control" rows="5"  id="description"><%= currentproduct.getDescription()%></textarea>
                <br>
                <button type="submit" class="btn btn-primary mb-2 mebtn">Edit Product</button>
            </form>
            <!-- This allows for the deletion of the current product being displayed -->
            <form action="DeleteProduct">
                <button class="btn btn-primary mb-2 mybtn">Delete Product</button>
            </form>
        </div>
        <style>
            .container{
                padding-bottom: 90px;
                width: 70%;
            }
            .mybtn{
                background-color: red;
            }
            .mebtn{
                background-color: green;
            }
        </style>
        <%@ include file="footer.jsp" %>
    </body>
</html>
