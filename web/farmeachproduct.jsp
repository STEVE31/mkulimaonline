<%@page import="entity.Farmer"%>
<%@page import="entity.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //checks whether within the session there is an attribute buy prod and vendor which are not null
    if(session.getAttribute("buyprod") == null && session.getAttribute("vendor") == null){
        response.sendRedirect("farmlogin.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="farmloginheader.jsp"%>
        <title>Each product</title>
    </head>
    <body>
        <div class="container">
            <%
                //casts the buy prod to an entity Product
                Product currentproduct = (Product) session.getAttribute("buyprod");
                //casts session attribute vendor into a string
                String vendor = (String) session.getAttribute("vendor");
                //changes the photo according to the type of current product that was casted
                if(currentproduct.getType().equals("Pineapples")){
            %>
                <img src="pineapple.jpg" height="300px" width="300px">
            <%
                }else if(currentproduct.getType().equals("Bananas")){
            %>
                <img src="banana.jpg" height="300px" width="300px">
            <%
                }else if(currentproduct.getType().equals("Avocado")){
            %>
                <img src="avocado.jpg" height="300px" width="300px">
            <%
                }
            %>
            <!-- product details are then displayed-->
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricing">
                    <h3 class="price">
                        Ksh.
                        <%=currentproduct.getPrice()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Product Name:<br>
                        <%=currentproduct.getName()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Units:
                        <%=currentproduct.getUnits()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Type:<br>
                        <%=currentproduct.getType()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Category:
                        <%=currentproduct.getCategory()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Vendor:
                        <%=vendor%>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>
                    Description
                    </h3>
                    <textarea name="description" rows="5" class="txt" disabled><%= currentproduct.getDescription()%></textarea>
                </div>
            </div>
            <!-- A button that allows the farmer to add products within the cart within the units range -->
            <form action="AddToCart">
                <input type="hidden" name="prodid" value="<%=currentproduct.getId()%>">
                <button class="btn btn-primary mb-2 mybtn">Add to Cart</button>
            </form>
        </div>
        <style>
            .pricing{
                background-color: darkgray;
            }
            .price{
                text-align:right;
                color: bisque;
            }
            .name{
                text-align:right;
            }
            .container{
                padding-bottom: 100px;
            }
            .txt{
                border: solid #cccccc;
                width: 100%;
                color: #999999;
            }
            .btn{
                width: 100%;
            }
        </style>
        <%@ include file="footer.jsp" %>
    </body>
</html>
