<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- pricing of the products can be viewed here with an embedded graph -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file = "farmloginheader.jsp" %>
        <title>Pricing</title>
    </head>
    <body>
        <div class="container meee"><h1>Pricing for Products</h1></div>
        <div class="container"><iframe width="1000" height="550" src="//embed.chartblocks.com/1.0/?c=5cb442ea3ba0f6ff04bc22e7&t=cf554ab61029746" frameBorder="0"></iframe></div>
        <%@include file="footer.jsp" %>
    </body>
    <style>
        .meee{
            border-radius: 7%;
            background-color: antiquewhite;
            text-align: center;
        }
        .container{
            width: 75%;
        }
    </style>
</html>
