<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- Footer that appears within all pages fixed to the bottom of the page -->
<div class="feet">
    <p>&copy; Copyright 2058, Mkulima Online</p>
    <p>Contact Us On:  
        <a href="#" class="fa fa-facebook"></a>Facebook
        <a href="#" class="fa fa-twitter"></a>Twitter
        <a href="#" class="fa fa-google"></a>Google
        <a href="#" class="fa fa-instagram"></a>Instagram
        <span class="glyphicon glyphicon-envelope"></span>254725603808
    </p>
</div>
<style>
    .feet {
      position: fixed;
      left: 0;
      bottom: 0;
      width: 100%;
      background-color: gainsboro;
      color: appworkspace;
      text-align: center;
    }    
</style>