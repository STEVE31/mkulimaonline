<%@page import="entity.Buyer"%>
<%@page import="java.util.*"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="entity.Product"%>
<%@page import="entity.Farmer"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="farmloginheader.jsp" %>
        <title>The shop</title>
    </head>
    <body>
        <%
            //stores the session useremail into a string
            String useremail = null;
            useremail = (String) session.getAttribute("farmuseremail");
        %>
        <h1>View and purchase some products</h1>
        <div class="container-fluid">
            <div id="accordion" class="accordion">
                <%
                //session factory and registry created
                SessionFactory sessionFactory;
                ServiceRegistry serviceRegistry;
                //configuring annotated classes
                Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Farmer.class)
                        .addAnnotatedClass(Product.class)
                        .addAnnotatedClass(Buyer.class)
                    .configure();
                //service registry creates a complete configuration from the previous one and adds hibernate.cfg,xml
                serviceRegistry = new ServiceRegistryBuilder()
                        .applySettings(configuration.getProperties())
                        .configure("hibernate.cfg.xml")
                        .build();
                //the session factory creates a configuration for a session from the service registry
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                //a session is opened with the session factory configuration
                Session sess = sessionFactory.openSession();
                //a query is created for the database
                String hql = "FROM Product";
                //the query is then configured into an sql hibernate query
                Query queryy = sess.createQuery(hql);
                List<Product> products;
                //the result list is stored in a list
                List results = queryy.list();
                // if the results are empty a message is displayed
                if (results == null || results.isEmpty()){
                    out.println("<h3>No products are currently available for purchase available</h3>");
                }else{
                    //the results are placed into a list of products
                    products = results;
                    //iterations to display products according to the types are created
                    //this also allows for the creation of a form for each product that is going to be displayed
                    //this form has a button that allows for the user to purchase products by adding them to a cart
                    Iterator<Product> itr = products.iterator();
                    Iterator<Product> itr1 = products.iterator();
                    Iterator<Product> itr2 = products.iterator();
                %>
                <div class="card mb-0">
                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                        <a class="card-title">
                            Pineapples
                        </a>
                    </div>
                    <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
                        <div class="row container">
                            <% 
                                int i = 0;
                                while(itr.hasNext()){
                                Product g = itr.next();
                                int prodid = g.getId();
                                    if(g.getType().equals("Pineapples") && !g.vendor.getEmail().equals(useremail)){
                                        i++;
                            %>
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="pineapple.jpg" alt="image" height="100px" width="100px">
                                    <p><%= g.getName()%></p>
                                    <p><%= g.vendor.getUsername()%></p>
                                    <form action="EachProd">
                                    <input type="hidden" name="prodid" value="<%=prodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            <%      }
                                }
                                if(i == 0){
                                    out.println("<p>No pineapples are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <a class="card-title">
                          Bananas
                        </a>
                    </div>
                    <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                        <div class="row container">
                            <%
                                int j = 0;
                                while(itr1.hasNext()){
                                Product g = itr1.next();
                                    int prodid = g.getId();
                                    if(g.getType().equals("Bananas") && !g.vendor.getEmail().equals(useremail)){
                                        j++;
                                %>
                                    <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                        <img src="banana.jpg" alt="image" height="100px" width="100px">
                                        <p><%= g.getName()%></p>
                                        <p><%= g.vendor.getUsername()%></p>
                                        <form action="EachProd">
                                        <input type="hidden" name="prodid" value="<%=prodid%>">
                                        <button type="submit" class="btn btn-primary">Purchase</button>
                                        </form>
                                    </div>
                            <%      }
                                } 
                                if(j == 0){
                                    out.println("<p>No bananas are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <a class="card-title">
                          Avocadoes
                        </a>
                    </div>
                    <div id="collapseThree" class="collapse card-body" data-parent="#accordion" >
                        <div class="row container">
                            <%  int k = 0;
                                while(itr2.hasNext()){
                                Product g = itr2.next();
                                int prodid = g.getId();
                                    if(g.getType().equals("Avocado") && !g.vendor.getEmail().equals(useremail)){
                                        k++;
                            %>
                                    <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                        <img src="avocado.jpg" alt="image" height="100px" width="100px">
                                        <p><%= g.getName()%></p>
                                        <p><%= g.vendor.getUsername()%></p>
                                        <form action="EachProd">
                                        <input type="hidden" name="prodid" value="<%=prodid%>">
                                        <button type="submit" class="btn btn-primary">Purchase</button>
                                        </form>
                                    </div>
                            <%      }
                                }
                                if(k == 0){
                                    out.println("<p>No bananas are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                    <%  }
                                        sess.close();
                                %>
        <style>
            .accordion .card-header:after {
                font-family: 'FontAwesome';  
                content: "\f068";
                float: right; 
                color: darkgray;
                padding-right: 15px;
            }
            .accordion .card-header.collapsed:after {
                /* symbol for "collapsed" panels */
                content: "\f067";
                color: darkgray;
                padding-right: 15px;
            }
            .card-header{
                color: bisque ;
                height: 40px ;
                background-color: beige ;
                border-radius: 5px;
            }
            a {
                padding-left: 10px ;
                padding-top: 30px;
                text-orientation: use-glyph-orientation ;
                text-decoration: none;
            }
            a:link, a:visited {
                color: inherit;
            }
            p{
                font-weight: bold;
            }
            .container-fluid{
                padding-bottom: 60px;
            }
        </style>
        <%@include file="footer.jsp" %>
    </body>
</html>
