<%@page import="org.apache.tomcat.util.codec.binary.Base64"%>
<%@page import="entity.Farmingproducts"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    if(session.getAttribute("farmeditable") == null){
        response.sendRedirect("login.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="farmloginheader.jsp"%>
        <title>Farming Each Product</title>
    </head>
    <body>
        <div class="container-fluid"><h4>Edit item within the cart by inputting number at the bottom</h4></div>
        <div class="container">
            <%
                //attribute farmeditable cast into an entity Farming products
                Farmingproducts currentprod = (Farmingproducts) session.getAttribute("farmeditable");
                //image of the current product is taken from the database as bytes
                byte[] bFile = currentprod.getImage();
                //the bytes are encoded into an encodebase64 
                byte[] encodeBase64 = Base64.encodeBase64(bFile);
                //the encoded encodebase64 is the converted into a string with UTF-8 encoding
                String base64DataString = new String(encodeBase64 , "UTF-8");
                System.out.println(currentprod.getUnits());
            %>
            <img src="data:image/png;base64, <%=base64DataString%>" height="300px" width="300px">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricing">
                    <h3 class="price">
                        Ksh.
                        <%=currentprod.getPrice()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Product Name:<br>
                        <%=currentprod.getName()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Units:
                        <%=currentprod.getUnits()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Category:
                        <%=currentprod.getCategory()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Vendor:
                        <%=currentprod.getVendor()%>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>
                    Description
                    </h3>
                    <textarea name="description" rows="5" class="txt" disabled><%= currentprod.getDescription()%></textarea>
                </div>
            </div>
            <!-- a form that allows one to input the product units within the range of the products within the database -->
            <!-- a button then allows the items for purchase to be increased -->
            <form action="FarmChangeCart">
                <div class="form-group">
                    <label for="units"><h3>Input a number within the units range</h3></label><br>
                    <label for="range"><h3>Between 1 and <%=currentprod.getUnits()%></h3></label>
                    <input type="number" name="farmunits" max="<%=currentprod.getUnits()%>" min="1" class="form-control mb-2 mr-sm-2" required>
                    <button class="btn btn-primary mb-2 mybtn">Edit Cart</button>
                </div>
            </form>
        </div>
        <style>
            .pricing{
                background-color: darkgray;
            }
            .price{
                text-align:right;
                color: bisque;
            }
            .name{
                text-align:right;
            }
            .container{
                padding-bottom: 100px;
            }
            .txt{
                border: solid #cccccc;
                width: 100%;
                color: #999999;
            }
            .btn{
                width: 100%;
            }
            .container-fluid{
                text-align: center;
                background-color: floralwhite;
                width: 50%;
            }
        </style>
        </div>
        <%@ include file="footer.jsp" %>
    </body>
</html>
