<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="farmloginheader.jsp" %>
        <title>Pests and Diseases</title>
    </head>
    <body>
        <div class="contsiner">
            <h2>General Information Including Pests and diseases</h2>
        </div>
        <div class="container-fluid">
            <div id="accordion" class="accordion">
                <div class="card mb-0">
                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                        <a class="card-title">
                            Pineapples
                        </a>
                    </div>
                    <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
                        <div class="row container">
                           <iframe width="100%" height="5000px" src="https://infonet-biovision.org/PlantHealth/Crops/Pineapple" frameBorder="0" allowfullscreen="allowfullscreen"></iframe>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <a class="card-title">
                          Bananas
                        </a>
                    </div>
                    <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                        <div class="row container">
                            <iframe width="100%" height="5000px" src="https://infonet-biovision.org/PlantHealth/Crops/Bananas" frameBorder="0" allowfullscreen="allowfullscreen"></iframe>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <a class="card-title">
                          Avocadoes
                        </a>
                    </div>
                    <div id="collapseThree" class="collapse card-body" data-parent="#accordion" >
                        <div class="row">
                           <iframe width="100%" height="5000px" src="https://infonet-biovision.org/PlantHealth/Crops/Avocados" frameBorder="0" allowfullscreen="allowfullscreen"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="footer.jsp" %>
    </body>
    <style>
        .accordion .card-header:after {
            font-family: 'FontAwesome';  
            content: "\f068";
            float: right; 
            color: darkgray;
            padding-right: 15px;
        }
        .accordion .card-header.collapsed:after {
            /* symbol for "collapsed" panels */
            content: "\f067";
            color: darkgray;
            padding-right: 15px;
        }
        .card-header{
            color: bisque ;
            height: 40px ;
            background-color: beige ;
            border-radius: 5px;
        }
        a {
            padding-left: 10px ;
            padding-top: 30px;
            text-orientation: use-glyph-orientation ;
            text-decoration: none;
        }
        a:link, a:visited {
            color: inherit;
        }
        p{
            font-weight: bold;
        }
        .container-fluid{
            padding-bottom: 60px;
        }
    </style>
</html>
