<%@page import="entity.Buyer"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="entity.Product"%>
<%@page import="entity.Farmer"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //Session attribute farm useremail is stored within a value
    String useremail = null;
    useremail = (String) session.getAttribute("farmuseremail");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="farmloginheader.jsp" %>
        <title>My products</title>
    </head>
        <div class = "container">
        <h1><%=useremail%> . Here are your products</h1>
        </div>
        <div class="container-fluid">
            <div class = "row">
            <%
                //session factory is created together with the the registry
                SessionFactory sessionFactory;
                ServiceRegistry serviceRegistry;
                //configuration of the annotated classes is then done
                Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Farmer.class)
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Buyer.class)
                    .configure();
                //service registry creates a full configuration adding hibernate configuration file and the previously
                //created configuration
                serviceRegistry = new ServiceRegistryBuilder()
                        .applySettings(configuration.getProperties())
                        .configure("hibernate.cfg.xml")
                        .build();
                //session factory created with the registry configuration
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                //a session is then opened
                Session sess = sessionFactory.openSession();
                //a query is then created by the user
                String hql = "FROM Product";
                //a high query language is created
                Query queryy = sess.createQuery(hql);
                List<Product> products;
                //results from the query execution are stored within a list 
                List results = queryy.list();
                if (results == null || results.isEmpty()){
                    out.println("<h3>No products available</h3>");
                }else{
                    //results are placed into a list of products
                    products = results;
                    // iterations to display the farming products that allows for the farmer for the farmer to access
                    //the edit products page is created
                    Iterator<Product> itr = products.iterator();
                    while(itr.hasNext()){
                        Product g = itr.next();
                        if(g.vendor.getEmail().equals(useremail)){
                            int prodid = g.getId();
                            if(g.getType().equals("Pineapples")){
            %>
            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                <img src="pineapple.jpg" alt="image" height="100px" width="100px">
                <p><%= g.getName()%></p>
                <p><%= g.vendor.getUsername()%></p>
                <form action="EditProd">
                <input type="hidden" name="prodid" value="<%=prodid%>">
                <button type="submit" class="btn btn-primary">Edit Product</button>
                </form>
            </div>
            <%
                }else if(g.getType().equals("Bananas")){
            %>
            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                <img src="banana.jpg" alt="image" height="100px" width="100px">
                <p><%= g.getName()%></p>
                <p><%= g.vendor.getUsername()%></p>
                <form action="EditProd">
                <input type="hidden" name="prodid" value="<%=prodid%>">
                <button type="submit" class="btn btn-primary">Edit Product</button>
                </form>
            </div>
            <%
                }else if(g.getType().equals("Avocado")){
            %>
            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                <img src="avocado.jpg" alt="image" height="100px" width="100px">
                <p><%= g.getName()%></p>
                <p><%= g.vendor.getUsername()%></p>
                <form action="EditProd">
                <input type="hidden" name="prodid" value="<%=prodid%>">
                <button type="submit" class="btn btn-primary">Edit Product</button>
                </form>
            </div>
            <%
                            }
                        }
                    }
                }
                sess.close();
            %>
            </div>
        </div>
        <style>
            p{
                font-weight: bold;
            }
            .container-fluid{
                padding-bottom: 40px;
            }
        </style>
    <%@ include file="footer.jsp" %>
</html>
