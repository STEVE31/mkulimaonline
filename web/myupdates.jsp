<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- this is the admin page that allows for the admin to post farming products -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Farming Products</title>
        <%@include file = "adminheader.jsp"%>
    </head>
    <body>
        <div class="container-fluid"><h1>Post Farming Products</h1></div>
        <div class="container">
            <form action="PostFarmProd">
                <img id="blah" src="product.png" alt="my image" /><br>
                <label for="file">Input image of the product:</label><br>
                <input class="filing" name="image" type='file' onchange="readURL(this);" accept="image/gif, image/jpeg, image/png" required>
                <!-- Java Script that allows for the user to input images for the products -->
                <script>
                    function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah')
                                    .attr('src', e.target.result);
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                </script>
                <br><br>
                <label for="name" class="mr-sm-2">Product name:</label>
                <input name="name" type="text" class="form-control mb-2 mr-sm-2" id="name" required>
                
                <label for="category">Select category:</label><br>
                <div class="container-fluid radios">
                    <label class="form-check-label"><input type="radio" name="category" value="FarmEquipment" checked>Farming Equipment</label><br>
                    <label class="form-check-label"><input type="radio" name="category" value="FarmTool">Farming Tool</label><br>
                    <label class="form-check-label"><input type="radio" name="category" value="Fertilizer">Fertilizer</label><br>
                    <label class="form-check-label"><input type="radio" name="category" value="Pesticides">Pesticides</label><br>
                    <label class="form-check-label"><input type="radio" name="category" value="Fungicides">Fungicides</label><br>
                    <label class="form-check-label"><input type="radio" name="category" value="Herbicides">Herbicides</label><br>
                    <label class="form-check-label"><input type="radio" name="category" value="FarmAccessories">Farming Accessories</label><br>
                </div>
                <br><br>
                <label for="vendor" class="mr-sm-2">Vendor Name:</label>
                <input name="vendor" type="text" class="form-control mb-2 mr-sm-2" id="vendor" required>
                <label for="price" class="mr-sm-2">Price:</label>
                <input name="price" type="number" min="1" class="form-control mb-2 mr-sm-2" id="price" required>
                <label for="units" class="mr-sm-2">Units:</label>
                <input name="units" type="number" min="1" class="form-control mb-2 mr-sm-2" id="units" required>
                <br><br>
                <label for="description">Description:</label>
                <textarea name="description" class="form-control" rows="5"  id="description" placeholder="input a product name that is small and describe the product fully here"></textarea>
                <br>
                <button type="submit" class="btn btn-primary mb-2">Post Farming Product</button>
            </form>
        </div>
        <style>
            .container{
                padding-bottom: 90px;
            }
            .form-check-label{
                background-color: #e7e7e7;
                width: 70%;
                height: 15%;
                border-radius: 10px;
            }
            .form-check-label:hover{
                background-color: #cccccc;
                width: 70%;
                height: 15%;
            }
            img{
                border-radius: 50%;
                width: 200px;
                height: 200px;
            }
            .filing{
                height: 30px;
                border-radius: 40%;
            }
        </style>
        <%@include file = "footer.jsp"%>
    </body>
</html>
