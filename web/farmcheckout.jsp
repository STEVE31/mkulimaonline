<%@page import="entity.Farmingproducts"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="entity.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        //stores the value of the farmuseremail within the session within a variable useremail
        String useremail = null;
        useremail = (String) session.getAttribute("farmuseremail");
        Product product;
        Farmingproducts farmproduct;
        int total = 0;
        int prodtotal = 0;
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="farmloginheader.jsp" %>
        <title>Farm Checkout</title>
    </head>
    <body>
        <%
            if((session.getAttribute("cart") == null || session.getAttribute("cart").equals(null) || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == "") && (session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals(null) || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || session.getAttribute("farmcart").toString() == "{}" || session.getAttribute("farmcart") == "")){
            %>
        <div class="container-fluid"><h3>No items are in the cart please add some to make purchases</h3></div>
        <%
            }else{
        %>
        <div class="container main">
            <table class="container">
                <tr><th>Name</th><th>Category</th><th>Units</th><th>Price</th><th>Units to buy</th><th>Total</th><th></th><th></th></tr>
        <%
                HashMap<Product, Integer> shoppingcart =  (HashMap<Product, Integer>) session.getAttribute("cart");
                if(session.getAttribute("cart") == null || session.getAttribute("cart").equals(null) || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == ""){
                } else{
                for(Map.Entry m:shoppingcart.entrySet()){
                    product = (Product) m.getKey();
                    int newer = (int) m.getValue();
                    prodtotal = newer * product.getPrice();
                    total += prodtotal;
        %>
                <tr>
                    <td><%=product.getName()%></td>
                    <td><%=product.getCategory()%></td>
                    <td><%=product.getUnits()%></td>
                    <td><%=product.getPrice()%></td>
                    <td><%=newer%></td>
                    <td><%=prodtotal%></td>
                    <td>
                        <form action="EditCart">
                            <input type="hidden" name="id" value="<%=product.getId()%>">
                            <button class="btn btn-primary mb-2 mybtn">Edit</button>
                        </form>
                    </td>
                    <td>
                        <form action="RemoveFromCart">
                            <input type="hidden" name="delid" value="<%=product.getId()%>">
                            <button class="btn btn-primary mb-2 mybtn">Delete</button>
                        </form>
                    </td>
                </tr>
        <%
                    }
                }
                System.out.println(total);
                HashMap<Farmingproducts, Integer> farmcart =  (HashMap<Farmingproducts, Integer>) session.getAttribute("farmcart");
                System.out.println(farmcart);
                if(session.getAttribute("farmcart") == null || session.getAttribute("farmcart").equals(null) || session.getAttribute("farmcart").equals("") || session.getAttribute("farmcart").equals("{}") || session.getAttribute("farmcart").toString() == "{}" || session.getAttribute("farmcart") == ""){
                }else{
                    for(Map.Entry g:farmcart.entrySet()){
                        farmproduct = (Farmingproducts) g.getKey();
                        int newer = (int) g.getValue();
                        prodtotal = newer * farmproduct.getPrice();
                        total += prodtotal;
        %>
                <tr>
                    <td><%=farmproduct.getName()%></td>
                    <td><%=farmproduct.getCategory()%></td>
                    <td><%=farmproduct.getUnits()%></td>
                    <td><%=farmproduct.getPrice()%></td>
                    <td><%=newer%></td>
                    <td><%=prodtotal%></td>
                    <td>
                        <form action="FarmEditCart">
                            <input type="hidden" name="farmid" value="<%=farmproduct.getId()%>">
                            <button class="btn btn-primary mb-2 mybtn">Edit</button>
                        </form>
                    </td>
                    <td>
                        <form action="RemoveFromFarmCart">
                            <input type="hidden" name="farmdelid" value="<%=farmproduct.getId()%>">
                            <button class="btn btn-primary mb-2 mybtn">Delete</button>
                        </form>
                    </td>
                </tr>
        
        <%
                    }
                }
                //confirms that the total is the combination of all totals 
                System.out.println(total);
                //sets session attriute farm total to the total
                session.setAttribute("farmtotal", total);
                if(total != 0){
        %>
        <a href="farmlocation.jsp"><div class="jumbotron"><h2>Checkout</h2></div></a>
        <%
                }
            }
        %>
            </table>
            <div class="container-fluid total"><h3>Total: Ksh.<%=total%></h3></div>
        </div>
        <style>
            th{
                width: 110px;
                font-size: 27px;
                text-align: center ;
                background-color: #e2e1e1 ;
            }
            td{
                width: 110px;
                font-size: 23px;
                text-align: center;
                border: solid #e2e1e1 1px;
            }
            .jumbotron{
                text-align: center ;
                position: fixed;
                bottom: 0;
                left: 0;
                width: 100% ;
                background-color: #999999 ;
                text-decoration: none;
            }
            .jumbotron:hover{
                background-color: #e7e7e7 ;
            }
            table{
                background-color: #f0eeee;
                border-radius: 3%;
            }
            .total{
                text-align: center ;
                margin-left: 6%;
                border-radius: 3%;
                background-color: #f0eeee;
                padding-bottom: 20% ;
            }
            .main{
                height: 100%;
            }
        </style>
        <%@include file="footer.jsp" %>
    </body>
</html>
