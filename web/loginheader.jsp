<%@page import="java.lang.String"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //header that spans over all the pages after the buyer has logged in
    //checks to ensure that the user has a session going on
    if(session.getAttribute("useremail") == null){
        out.println("<script type=\"text/javascript\">");
        out.println("alert('You need to login as a buyer first!!');");
        out.println("window.location.href = \"login.jsp\";");
        out.println("</script>");
    }
%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<nav class="navbar navbar-inverse fixed-top">
  <div class="container-fixed">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.jsp">MkulimaOnline</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="shop.jsp">Shop</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span><%=session.getAttribute("useremail")%></a></li>
        <li><a href="checkout.jsp"><span class="glyphicon glyphicon-shopping-cart"></span></a>
        <li>
            <a href="#">
                <form action="LogOut">
                    <button type="submit" class="btn btn-primary">Logout <span class="glyphicon glyphicon-log-out"></span></button>
                </form>
            </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<style>
    nav{
        position: absolute;
    }
    .btn{
        background-color: black;
    }
    .btn:hover{
        background-color: black;
    }
</style>