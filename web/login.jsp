<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        //checks whether there is a user within the sessions and redirects them accordingly
        if(session.getAttribute("useremail") != null){
            response.sendRedirect("shop.jsp");
        }else if(session.getAttribute("farmuseremail") != null){
            response.sendRedirect("myproducts.jsp");
        }
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="header.jsp" %>
        <title>Login</title>
    </head>
    <!-- validation of the form is done to ensure fve characters are input in the password field -->
    <script>
        function validateForm() {
            var password = document.forms["logform"]["password"];
            if (password.value.length < 5) {
                alert("Password should atleast be 5 characters");
                return false;
            }else {
                return true;
            }
        } 
    </script>
    <body>
        <div class="container">
            <form name="logform" action="Login" onsubmit="return validateForm()">
            <div class="form-group">
                <label for="email">Email address:</label>
                <input name="email" type="email" class="form-control" id="email">
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <input name="password" type="password" class="form-control" id="pwd">
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox"> Remember me
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <p>Do not have an account?<a href="register.jsp">Sign Up</a></p>
        </div>
        <style>
            .container{
                width: 50%;
                padding: 90px 70px 90px 70px;
                border-left: 1px solid cornsilk;
                border-right: 1px solid cornsilk;
            }
            a {
            text-decoration: none;
            }
            a:link, a:visited {
                color: black;
            }
            p{
                text-align: center;
            }
        </style>
        <%@ include file="footer.jsp" %>
    </body>
</html>
