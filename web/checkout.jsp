<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="entity.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        //creates a string useremail and stores the value of session attribute useremail
        String useremail = null;
        useremail = (String) session.getAttribute("useremail");
        Product product;
        int total = 0;
        int prodtotal = 0;
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- includes the page when the page is being displayed -->
        <%@ include file="loginheader.jsp" %>
        <title>Checkout</title>
    </head>
    <body>
        <%
            //checking whether the shopping cart contains products
            if(session.getAttribute("cart") == null || session.getAttribute("cart").equals(null) || session.getAttribute("cart").equals("") || session.getAttribute("cart").equals("{}") || session.getAttribute("cart").toString() == "{}" || session.getAttribute("cart") == ""){
            %>
        <div class="container-fluid"><h3>No items are in the cart please add some to make purchases</h3></div>
        <%
            }else{ 
            //if the cart exists do iterations to create table for the products
            //Creating iterations for the checkout table
        %>
        <div class="container main">
            <table class="container">
                <tr><th>Name</th><th>Category</th><th>Type</th><th>Units</th><th>Price</th><th>Units to buy</th><th>Total</th><th></th><th></th></tr>
        <%
                HashMap<Product, Integer> shoppingcart =  (HashMap<Product, Integer>) session.getAttribute("cart");
                for(Map.Entry m:shoppingcart.entrySet()){
                    //the products are stored in a hashmap therefore we need to get each value and key
                    //the value is the product and the key is the number of units that were placed by the buyer
                    product = (Product) m.getKey();
                    int newer = (int) m.getValue();
                    //this helps in getting the total of the products and the units therefore the total for a single product
                    prodtotal = newer * product.getPrice();
                    //this helps us get the total for all products adding to itself after every iteration
                    total += prodtotal;
        %>
                <tr>
                    <td><%=product.getName()%></td>
                    <td><%=product.getCategory()%></td>
                    <td><%=product.getType()%></td>
                    <td><%=product.getUnits()%></td>
                    <td><%=product.getPrice()%></td>
                    <td><%=newer%></td>
                    <td><%=prodtotal%></td>
                    <td>
                        <form action="EditCart">
                            <input type="hidden" name="id" value="<%=product.getId()%>">
                            <button class="btn btn-primary mb-2 mybtn">Edit</button>
                        </form>
                    </td>
                    <td>
                        <form action="RemoveFromCart">
                            <input type="hidden" name="delid" value="<%=product.getId()%>">
                            <button class="btn btn-primary mb-2 mybtn">Delete</button>
                        </form>
                    </td>
                </tr>
        <%
                }
                //This confirms that the total is the combination of all totals
                System.out.println(total);
                //sets session attriute farm total to the total
                session.setAttribute("total", total);
                if(total != 0){
        %>
        <a href="location.jsp"><div class="jumbotron"><h2>Checkout</h2></div></a>
        <%      }
            }
        %>
            </table>
            <div class="container-fluid total"><h3>Total: Ksh.<%=total%></h3></div>
        </div>
        <style>
            th{
                width: 110px;
                font-size: 27px;
                text-align: center ;
                background-color: #e2e1e1 ;
            }
            td{
                width: 110px;
                font-size: 23px;
                text-align: center;
                border: solid #e2e1e1 1px;
            }
            .jumbotron{
                text-align: center ;
                position: fixed;
                bottom: 0;
                left: 0;
                width: 100% ;
                background-color: #999999 ;
                text-decoration: none;
            }
            .jumbotron:hover{
                background-color: #e7e7e7 ;
            }
            table{
                background-color: #f0eeee;
                border-radius: 3%;
            }
            .total{
                text-align: center ;
                margin-left: 6%;
                border-radius: 3%;
                background-color: #f0eeee;
            }
            .main{
                height: 100%;
            }
        </style>
        <%@include file="footer.jsp" %>
    </body>
</html>
