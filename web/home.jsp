<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Initial page displaying the details of the user functionalities -->
<!DOCTYPE html>
    <div class="jumbotron">
        <h1>
            Mkulima Online
        </h1>
        <h3>
            Buy and sell farm produce directly to willing buyers<br>
            Purchase and get information about farm tools<br>
            Enter as a farmer to sell products and as a buyer to purchase farm produce
        </h3>
    </div>
    <div class="container">
      <div class="row">
        <a href="farmlogin.jsp">
            <div class="col-sm-5">
                <h3>Farmer</h3>
                <p>Post Goods</p>
                <p>Buy farm devices and fertilizers</p>
                <p>View farming systems and pest and disease information</p>
            </div>
        </a>
        <a href="login.jsp">
            <div class="col-sm-7">
                <h3>Buyer</h3>
                <p>View and purchase farm products</p>
                <p>Make orders</p>
            </div>
        </a>
      </div>
    </div>
    <style>
        .col-sm-7{
            text-align: right;
        }
        .col-sm-7:hover{
            background-color: beige;
            cursor: pointer;
        }
        .col-sm-5:hover{
            background-color: beige;
            cursor: pointer;
        }
        a {
        text-decoration: none;
        }
        a:link, a:visited {
            color: black;
        }
        .row{
            padding-bottom: 50px;
        }
    </style>
