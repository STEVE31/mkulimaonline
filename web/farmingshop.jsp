<%@page import="entity.Buyer"%>
<%@page import="org.apache.tomcat.util.codec.binary.Base64"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.awt.image.BufferedImage"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="entity.Farmingproducts"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="entity.Product"%>
<%@page import="entity.Farmer"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="farmloginheader.jsp"%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Farming Shop</title>
    </head>
    <body>
        <div class="container"><h1>Purchase Some Farming Products</h1></div>
        <div class="container-fluid">
            <div id="accordion" class="accordion">
                <%
                    //creates a session factory and a service registry 
                    SessionFactory sessionFactory;
                    ServiceRegistry serviceRegistry;
                    //creates a configuration that allows for the adding of the annotated classes 
                    //annotated classes help in the storage of the objects in the database 
                    Configuration configuration = new Configuration();
                    configuration.addAnnotatedClass(Farmer.class)
                            .addAnnotatedClass(Product.class)
                            .addAnnotatedClass(Farmingproducts.class)
                            .addAnnotatedClass(Buyer.class)
                        .configure();
                    //the service registry takes up the new configuration that has been made (annoteted classes) 
                    //then then gets the current configuration within the hibernate.cfg,xml and build using both configurations 
                    serviceRegistry = new ServiceRegistryBuilder()
                            .applySettings(configuration.getProperties())
                            .configure("hibernate.cfg.xml")
                            .build(); 
                    //a session factory is created with the above configuration 
                    sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                    //a session is then created with the configuration
                    Session sess = sessionFactory.openSession();
                    //a query that selects every product is created
                    String hql = "FROM Farmingproducts";
                    //the query is then used to get the values from the database
                    Query queryy = sess.createQuery(hql);
                    List<Farmingproducts> products;
                    //the results are stored within a list
                    List results = queryy.list();
                    //the results are checked to ensure that they are not empty and a message is displayed in case of the results being empty
                    if (results == null || results.isEmpty()){
                    out.println("<h3>No farming products are currently available for purchase available</h3>");
                    }else{
                        //iterations according to all product categories is created in order to display all products and the stored images
                        //a button that allows for the farmer to purchase the products is then created
                        products = results;
                        Iterator<Farmingproducts> itr = products.iterator();
                        Iterator<Farmingproducts> itr1 = products.iterator();
                        Iterator<Farmingproducts> itr2 = products.iterator();
                        Iterator<Farmingproducts> itr3 = products.iterator();
                        Iterator<Farmingproducts> itr4 = products.iterator();
                        Iterator<Farmingproducts> itr5 = products.iterator();
                        Iterator<Farmingproducts> itr6 = products.iterator();
                %>
                <div class="card mb-0">
                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                        <a class="card-title">
                            Farm Equipment
                        </a>
                    </div>
                    <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
                        <div class="row container">
                            <%
                                int a = 0;
                                while(itr.hasNext()){
                                    Farmingproducts m = itr.next();
                                    if(m.getCategory().equals("FarmEquipment")){
                                        int farmprodid = m.getId();
                                        byte[] bFile = m.getImage();
                                        byte[] encodeBase64 = Base64.encodeBase64(bFile);
                                        String base64DataString = new String(encodeBase64 , "UTF-8");
                                        a++;
                            %>
                            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="data:image/png;base64, <%=base64DataString%>" alt="image" height="100px" width="100px">
                                    <p><%= m.getName()%></p>
                                    <p><%= m.getVendor() %></p>
                                    <form action="EachFarmingProd">
                                    <input type="hidden" name="prodid" value="<%=farmprodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            </div>
                            <% 
                                    }
                                }
                                if(a == 0){
                                    out.println("<p>No farming equipment are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <a class="card-title">
                          Farming Tool
                        </a>
                    </div>
                    <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                        <div class="row container">
                            <%
                                int b = 0;
                                while(itr1.hasNext()){
                                    Farmingproducts m = itr1.next();
                                    if(m.getCategory().equals("FarmTool")){
                                        int farmprodid = m.getId();
                                        byte[] bFile = m.getImage();
                                        byte[] encodeBase64 = Base64.encodeBase64(bFile);
                                        String base64DataString = new String(encodeBase64 , "UTF-8");
                                        b++;
                            %>
                            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="data:image/png;base64, <%=base64DataString%>" alt="image" height="100px" width="100px">
                                    <p><%= m.getName()%></p>
                                    <p><%= m.getVendor() %></p>
                                    <form action="EachFarmingProd">
                                    <input type="hidden" name="prodid" value="<%=farmprodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            </div>
                            <% 
                                    }
                                }
                                if(b == 0){
                                    out.println("<p>No farming tools are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <a class="card-title">
                          Fertilizer
                        </a>
                    </div>
                    <div id="collapseThree" class="collapse card-body" data-parent="#accordion" >
                        <div class="row container">
                            <%
                                int c = 0;
                                while(itr2.hasNext()){
                                    Farmingproducts m = itr2.next();
                                    if(m.getCategory().equals("Fertilizer")){
                                        int farmprodid = m.getId();
                                        byte[] bFile = m.getImage();
                                        byte[] encodeBase64 = Base64.encodeBase64(bFile);
                                        String base64DataString = new String(encodeBase64 , "UTF-8");
                                        c++;
                            %>
                            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="data:image/png;base64, <%=base64DataString%>" alt="image" height="100px" width="100px">
                                    <p><%= m.getName()%></p>
                                    <p><%= m.getVendor() %></p>
                                    <form action="EachFarmingProd">
                                    <input type="hidden" name="prodid" value="<%=farmprodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            </div>
                            <% 
                                    }
                                }
                                if(c == 0){
                                    out.println("<p>No fertilizers are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                        <a class="card-title">
                          Pesticides
                        </a>
                    </div>
                    <div id="collapseFour" class="collapse card-body" data-parent="#accordion" >
                        <div class="row container">
                            <%
                                int d = 0;
                                while(itr3.hasNext()){
                                    Farmingproducts m = itr3.next();
                                    if(m.getCategory().equals("Pesticides")){
                                        int farmprodid = m.getId();
                                        byte[] bFile = m.getImage();
                                        byte[] encodeBase64 = Base64.encodeBase64(bFile);
                                        String base64DataString = new String(encodeBase64 , "UTF-8");
                                        d++;
                            %>
                            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="data:image/png;base64, <%=base64DataString%>" alt="image" height="100px" width="100px">
                                    <p><%= m.getName()%></p>
                                    <p><%= m.getVendor() %></p>
                                    <form action="EachFarmingProd">
                                    <input type="hidden" name="prodid" value="<%=farmprodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            </div>
                            <% 
                                    }
                                }
                                if(d == 0){
                                    out.println("<p>No pesticides are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                        <a class="card-title">
                          Fungicides
                        </a>
                    </div>
                    <div id="collapseFive" class="collapse card-body" data-parent="#accordion" >
                        <div class="row container">
                            <%
                                int f = 0;
                                while(itr4.hasNext()){
                                    Farmingproducts m = itr4.next();
                                    if(m.getCategory().equals("Fungicides")){
                                        int farmprodid = m.getId();
                                        byte[] bFile = m.getImage();
                                        byte[] encodeBase64 = Base64.encodeBase64(bFile);
                                        String base64DataString = new String(encodeBase64 , "UTF-8");
                                        f++;
                            %>
                            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="data:image/png;base64, <%=base64DataString%>" alt="image" height="100px" width="100px">
                                    <p><%= m.getName()%></p>
                                    <p><%= m.getVendor() %></p>
                                    <form action="EachFarmingProd">
                                    <input type="hidden" name="prodid" value="<%=farmprodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            </div>
                            <% 
                                    }
                                }
                                if(f == 0){
                                    out.println("<p>No fungicides are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                        <a class="card-title">
                          Herbicides
                        </a>
                    </div>
                    <div id="collapseSix" class="collapse card-body" data-parent="#accordion" >
                        <div class="row container">
                            <%
                                int g = 0;
                                while(itr5.hasNext()){
                                    Farmingproducts m = itr5.next();
                                    if(m.getCategory().equals("Herbicides")){
                                        int farmprodid = m.getId();
                                        byte[] bFile = m.getImage();
                                        byte[] encodeBase64 = Base64.encodeBase64(bFile);
                                        String base64DataString = new String(encodeBase64 , "UTF-8");
                                        g++;
                            %>
                            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="data:image/png;base64, <%=base64DataString%>" alt="image" height="100px" width="100px">
                                    <p><%= m.getName()%></p>
                                    <p><%= m.getVendor() %></p>
                                    <form action="EachFarmingProd">
                                    <input type="hidden" name="prodid" value="<%=farmprodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            </div>
                            <% 
                                    }
                                }
                                if(g == 0){
                                    out.println("<p>No herbicides are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                        <a class="card-title">
                          Farming Accessories
                        </a>
                    </div>
                    <div id="collapseSeven" class="collapse card-body" data-parent="#accordion" >
                        <div class="row container">
                            <%
                                int h = 0;
                                while(itr6.hasNext()){
                                    Farmingproducts m = itr6.next();
                                    if(m.getCategory().equals("FarmAccessories")){
                                        int farmprodid = m.getId();
                                        byte[] bFile = m.getImage();
                                        byte[] encodeBase64 = Base64.encodeBase64(bFile);
                                        String base64DataString = new String(encodeBase64 , "UTF-8");
                                        h++;
                            %>
                            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="data:image/png;base64, <%=base64DataString%>" alt="image" height="100px" width="100px">
                                    <p><%= m.getName()%></p>
                                    <p><%= m.getVendor() %></p>
                                    <form action="EachFarmingProd">
                                    <input type="hidden" name="prodid" value="<%=farmprodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            </div>
                            <% 
                                    }
                                }
                                if(h == 0){
                                    out.println("<p>No farming accessories are currently available for purchase available</p>");
                                }
                            %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                    <%  }
                                        sess.close();
                                %>
        <style>
            .accordion .card-header:after {
                font-family: 'FontAwesome';  
                content: "\f068";
                float: right; 
                color: darkgray;
                padding-right: 15px;
            }
            .accordion .card-header.collapsed:after {
                /* symbol for "collapsed" panels */
                content: "\f067";
                color: darkgray;
                padding-right: 15px;
            }
            .card-header{
                color: bisque ;
                height: 40px ;
                background-color: beige ;
                border-radius: 5px;
            }
            a {
                padding-left: 10px ;
                padding-top: 30px;
                text-orientation: use-glyph-orientation ;
                text-decoration: none;
            }
            a:link, a:visited {
                color: inherit;
            }
            p{
                font-weight: bold;
            }
            .container-fluid{
                padding-bottom: 60px;
            }
        </style>
        <%@include file="footer.jsp"%>
    </body>
</html>
