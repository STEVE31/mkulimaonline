<%@page import="entity.Buyer"%>
<%@page import="java.util.*"%>
<%@page import="org.hibernate.Query"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.service.ServiceRegistryBuilder"%>
<%@page import="entity.Product"%>
<%@page import="entity.Farmer"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.service.ServiceRegistry"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- allows for the buyer to purchase the products being displayed and viewing the products -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="loginheader.jsp" %>
        <title>The shop</title>
    </head>
    <body>
        <h1>View and purchase some products</h1>
        <!-- an accordion is used to display the products according to their types for the fruit category -->
        <div class="container-fluid">
            <div id="accordion" class="accordion">
                <%
                //session factory and service registry are created 
                SessionFactory sessionFactory;
                ServiceRegistry serviceRegistry;
                //a new configuration is created adding all the annotated classes created
                Configuration configuration = new Configuration();
                configuration.addAnnotatedClass(Farmer.class)
                        .addAnnotatedClass(Product.class)
                        .addAnnotatedClass(Buyer.class)
                    .configure();
                //tha new configuration is combined with tha hibernate configuration within the service registry
                serviceRegistry = new ServiceRegistryBuilder()
                        .applySettings(configuration.getProperties())
                        .configure("hibernate.cfg.xml")
                        .build();        
                //a session factory is built using the service registry with the configurations
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
                //creating a session using the complete configuration
                Session sess = sessionFactory.openSession();
                //query is created but as a string and then converted to high query language and the database queried 
                String hql = "FROM Product";
                Query queryy = sess.createQuery(hql);
                List<Product> products;
                //list is used to store the results
                List results = queryy.list();
                if (results == null || results.isEmpty()){
                    out.println("<h3>No products are currently available for purchase available</h3>");
                }else{
                    //iterationa are then used to create viewa for tha products
                    products = results;
                    Iterator<Product> itr = products.iterator();
                    Iterator<Product> itr2 = products.iterator();
                    Iterator<Product> itr3 = products.iterator();
                %>
                <div class="card mb-0">
                    <div class="card-header collapsed" data-toggle="collapse" href="#collapseOne">
                        <a class="card-title">
                            Pineapples
                        </a>
                    </div>
                    <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
                        <div class="row container">
                            <%while(itr.hasNext()){
                                Product g = itr.next();
                                int prodid = g.getId();
                                    if(g.getType().equals("Pineapples")){
                            %>
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="pineapple.jpg" alt="image" height="100px" width="100px">
                                    <p><%= g.getName()%></p>
                                    <p><%= g.vendor.getUsername()%></p>
                                    <form action="EachProd">
                                    <input type="hidden" name="prodid" value="<%=prodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                            <% }
                              }%>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <a class="card-title">
                          Bananas
                        </a>
                    </div>
                    <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                        <div class="row container">
                            <%while(itr2.hasNext()){
                                Product g = itr2.next();
                                int prodid = g.getId();
                                if(g.getType().equals("Bananas")){
                                %>
                            <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                <img src="banana.jpg" alt="image" height="100px" width="100px">
                                <p><%= g.getName()%></p>
                                <p><%= g.vendor.getUsername()%></p>
                                <form action="EachProd">
                                <input type="hidden" name="prodid" value="<%=prodid%>">
                                <button type="submit" class="btn btn-primary">Purchase</button>
                                </form>
                            </div>
                            <% }
                                }%>
                        </div>
                    </div>
                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <a class="card-title">
                          Avocadoes
                        </a>
                    </div>
                    <div id="collapseThree" class="collapse" data-parent="#accordion" >
                        <div class="card-body">
                            <div class="row container">
                                <%while(itr3.hasNext()){
                                    Product g = itr3.next();
                                    int prodid = g.getId();
                                        if(g.getType().equals("Avocado")){
                                %>
                                <div class="col-sm-2 col-xs-6 col-md-2 col-lg-1">
                                    <img src="avocado.jpg" alt="image" height="100px" width="100px">
                                    <p><%= g.getName()%></p>
                                    <p><%= g.vendor.getUsername()%></p>
                                    <form action="EachProd">
                                    <input type="hidden" name="prodid" value="<%=prodid%>">
                                    <button type="submit" class="btn btn-primary">Purchase</button>
                                    </form>
                                </div>
                                <%  }
                                     }%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                    <%    }
                                            sess.close();
                                %>
        <style>
            .accordion .card-header:after {
                font-family: 'FontAwesome';  
                content: "\f068";
                float: right; 
                color: darkgray;
                padding-right: 15px;
            }
            .accordion .card-header.collapsed:after {
                /* symbol for "collapsed" panels */
                content: "\f067";
                color: darkgray;
                padding-right: 15px;
            }
            .card-header{
                color: bisque ;
                height: 40px ;
                background-color: beige ;
                border-radius: 5px;
            }
            a {
                padding-left: 10px ;
                padding-top: 30px;
                text-orientation: use-glyph-orientation ;
                text-decoration: none;
            }
            a:link, a:visited {
                color: inherit;
            }
            p{
                font-weight: bold;
            }
            .container-fluid{
                padding-bottom: 60px;
            }
        </style>
        <%@include file="footer.jsp" %>
    </body>
</html>
