<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- Farmers are able to post their products according to the three types of fruits -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="farmloginheader.jsp" %>
        <title>Post Products</title>
    </head>
    <body>
        <div class="container-fluid"><h1>Post some products</h1></div>
        <div class="container">
            <form action="PostProd">
                <label for="name" class="mr-sm-2">Product name:</label>
                <input name="name" type="text" class="form-control mb-2 mr-sm-2" id="name" required>
                
                <label for="category">Select category:</label>
                <select name="category" class="form-control" id="category" required>
                    <option value="Fruit">Fruit</option>
                </select>
                
                <label for="type">Select type:</label>
                <select name="type" class="form-control" id="type" required>
                    <option value="Pineapples">Pineapple</option>
                    <option value="Bananas">Banana</option>
                    <option value="Avocado">Avocado</option>
                </select>
                <br><br>
                <label for="price" class="mr-sm-2">Price:</label>
                <input name="price" type="number" min="1" class="form-control mb-2 mr-sm-2" id="price" required>
                <label for="units" class="mr-sm-2">Units:</label>
                <input name="units" type="number" min="1" class="form-control mb-2 mr-sm-2" id="units" required>
                <br><br>
                <label for="description">Description:</label>
                <textarea name="description" class="form-control" rows="5"  id="description"></textarea>
                <br>
                <button type="submit" class="btn btn-primary mb-2">Post Product</button>
            </form>
        </div>
        <style>
            .container{
                padding-bottom: 90px;
                width: 70%;
            }
        </style>
        <%@ include file="footer.jsp" %>
    </body>  
</html>
