<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- A header for the pages before logging in within the farmer's side containing bootstrap -->
<meta http-equiv="cache-control" content="no-cache, must-revalidate, post-check=0, pre-check=0" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<nav class="navbar navbar-inverse fixed-top">
  <div class="container-fixed">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.jsp">MkulimaOnline</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="farmshop.jsp">Shop</a></li> 
        <li><a href="postprod.jsp">Post product</a></li>
        <li><a href="myproducts.jsp">My products</a></li> 
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="farmregister.jsp"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="farmlogin.jsp"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </li>
      </ul>
    </div>
  </div>
</nav>
<style>
    nav{
        position: absolute;
    }
</style>