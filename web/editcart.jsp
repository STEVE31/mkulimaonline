<%@page import="entity.Farmer"%>
<%@page import="entity.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //checks for attributes within the session and makes sure that they are not null 
    if(session.getAttribute("editable") == null && session.getAttribute("editvendor") == null){
        response.sendRedirect("login.jsp");
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="loginheader.jsp"%>
        <title>Each product</title>
    </head>
    <body>
        <div class="container-fluid"><h4>Edit item within the cart by inputting number at the bottom</h4></div>
        <div class="container">
            <%
                //casts a session attribute editable to an entity product
                Product currentproduct = (Product) session.getAttribute("editable");
                //casts a session attribute edit vendor to a string vendor
                String vendor = (String) session.getAttribute("editvendor");
                //changes the pictures according to the product type
                //product details are then displayed
                if(currentproduct.getType().equals("Pineapples")){
            %>
                <img src="pineapple.jpg" height="300px" width="300px">
            <%
                }else if(currentproduct.getType().equals("Bananas")){
            %>
                <img src="banana.jpg" height="300px" width="300px">
            <%
                }else if(currentproduct.getType().equals("Avocado")){
            %>
                <img src="avocado.jpg" height="300px" width="300px">
            <%
                }
            %>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 pricing">
                    <h3 class="price">
                        Ksh.
                        <%=currentproduct.getPrice()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Product Name:<br>
                        <%=currentproduct.getName()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Units:
                        <%=currentproduct.getUnits()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Type:<br>
                        <%=currentproduct.getType()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Category:
                        <%=currentproduct.getCategory()%>
                    </h3>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <h3 class="name">
                        Vendor:
                        <%=vendor%>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>
                    Description
                    </h3>
                    <textarea name="description" rows="5" class="txt" disabled><%= currentproduct.getDescription()%></textarea>
                </div>
            </div>
            <!-- A form that allows for changing of the units is created -->
            <!-- It takes in the new value that must be within the units within the database -->
            <form action="ChangeCart">
                <div class="form-group">
                    <label for="units"><h3>Input a number within the units range</h3></label><br>
                    <label for="range"><h3>Between 1 and <%=currentproduct.getUnits()%></h3></label>
                    <input type="number" name="units" max="<%=currentproduct.getUnits()%>" min="1" class="form-control mb-2 mr-sm-2" required>
                    <button class="btn btn-primary mb-2 mybtn">Edit Cart</button>
                </div>
            </form>
        </div>
        <style>
            .pricing{
                background-color: darkgray;
            }
            .price{
                text-align:right;
                color: bisque;
            }
            .name{
                text-align:right;
            }
            .container{
                padding-bottom: 100px;
            }
            .txt{
                border: solid #cccccc;
                width: 100%;
                color: #999999;
            }
            .btn{
                width: 100%;
            }
            .container-fluid{
                text-align: center;
                background-color: floralwhite;
                width: 50%;
            }
        </style>
        <%@ include file="footer.jsp" %>
    </body>
</html>
