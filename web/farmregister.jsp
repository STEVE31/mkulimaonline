<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        //checks whether there is a user with a session that is going on and for the farmer it redirects them to the my products page
        //a buyer is redirected to the useremail
        if(session.getAttribute("useremail") != null){
            response.sendRedirect("shop.jsp");
        }else if(session.getAttribute("farmuseremail") != null){
            response.sendRedirect("myproducts.jsp");
        }
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ include file="farmheader.jsp" %>
        <title>Farmer Registration</title>
    </head>
    <!-- a validation for the form is done using the java script code -->
    <script>
        function validateForm() {
            var fname = document.forms["regform"]["fname"];
            var lname = document.forms["regform"]["lname"];
            var password = document.forms["regform"]["password"];
            var username = document.forms["regform"]["username"];
            var phoneno = document.forms["regform"]["phoneno"];
            if (password.value.length < 5) {
                alert("Password should atleast be 5 characters");
                return false;
            }else if (username.value.length < 5) {
                alert("Username should atleast be 5 characters");
                return false;
            }else if (fname.value.length < 3) {
                alert("First name should atleast be 2 characters");
                return false;
            }else if (lname.value.length < 4) {
                alert("Last name should atleast be 4 characters");
                return false;
            }else if (phoneno.value.length != 9) {
                alert("Phone number contains 9 characters");
                return false;
            }else {
                return true;
            }
        } 
    </script>
    <body>
        <div class="container">
            <form name="regform" action="farmRegister" onsubmit="return validateForm()">
            <div class="form-group">
                <label for="email">Email address:</label>
                <input name="email" type="email" class="form-control" id="email" minlength="9" required>
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <input name="password" type="password" class="form-control" id="password" minlength="5" autocomplete="off" required>
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input name="username" type="text" class="form-control" id="username" minlength="5" required>
            </div>
            <div class="form-group">
                <label for="fname">First Name</label>
                <input name="fname" type="text" class="form-control" id="fname" minlength="3" required>
            </div>
            <div class="form-group">
                <label for="lname">Last name</label>
                <input name="lname" type="text" class="form-control" id="lname" minlength="4" required>
            </div>
            <div class="form-group">
                <label for="phoneno">Phone no.</label><br>
                +254<input name="phoneno" type="number" class="form-control" id="phoneno" size="9" required>
            </div>
            <div class="form-group">
                <label for="sel1">Select list:</label>
                <select name="location" class="form-control" id="location" required>
                    <option value="Nakuru">Nakuru</option>
                    <option value="Nairobi">Nairobi</option>
                    <option value="Nyahururu">Nyahururu</option>
                </select>
            </div>
            <div class="form-group form-check">
                <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Remember me
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <div class="align">
            <p>Already have an account?<a href="farmlogin.jsp">Login</a></p>
            </div>
        </div>
        <style>
            .container{
                width: 50%;
                align: center;
                border-left: 1px solid cornsilk;
                border-right: 1px solid cornsilk;
                margin-bottom: 30px;
            }
            .align{
                text-align: center;
            }
        </style>
        <%@ include file="footer.jsp" %>
    </body>
</html>
