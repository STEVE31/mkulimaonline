<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="farmloginheader.jsp" %>
        <title>Farm pickup location</title>
    </head>
    <body>
        <form action="FarmOrder" class="container locale">
            <div class="container-fluid radios">
                <label class="form-check-label"><input type="radio" name="locale" value="Nairobi" checked>Nairobi<br>
                    <h5> The offices are within the CBD along in Kimathi street within the Old Mutual Building </h5>
                </label>
                <label class="form-check-label"><input type="radio" name="locale" value="Nakuru">Nakuru<br>
                    <h5> The offices are within Pincam Building near Biashara Center </h5>
                </label>
                <label class="form-check-label"><input type="radio" name="locale" value="Nyahururu">Nyahururu<br>
                    <h5> The offices are within Bettan Hotel Building</h5>
                </label><br>
            </div>
            <button type="submit" class="btn btn-primary">Place order</button>
        </form>
        <%@include file="footer.jsp" %>
    </body>
    <style>
        .locale{
            height: 400px ;
            width: 500px ;
            background-color: #f0eeee;
            border-radius: 20px;
            margin-top: 5% ;
        }
        .locale:hover{
            height: 410px ;
            width: 550px ;
            background-color: beige ;
        }
        .btn{
            width: 100%;
            border-radius: 5px;
        }
        .form-check-label{
            height: 100px;
            width: 100%;
            border-radius: 20px ;
        }
        .form-check-label:hover{
            background-color: antiquewhite;
        }
    </style>
</html>
