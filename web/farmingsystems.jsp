<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- A file of a farmer that has logged in is created -->
        <%@include file = "farmloginheader.jsp" %>
        <title>Farming Systems</title>
    </head>
    <body>
        <div class="container-fluid ala">
            <div class="container meee"><h1>Farming Systems for all fruits</h1></div>
            <!-- A web site is embedded within the page containing farming system information -->
            <iframe width="100%" height="15000px" src="https://www.britannica.com/topic/fruit-farming" frameBorder="0" allowfullscreen="allowfullscreen"></iframe>
        </div>
        <%@include file="footer.jsp" %>
    </body>
    <style>
        .meee{
            border-radius: 5%;
            background-color: beige;
        }
        .ala{
            padding-left: 7% ;
            padding-right: 2% ;
        }
    </style>
</html>
