<%@page import="java.lang.String"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //checks whether there is a farmer account with a session within the system
    //this header spans over all the pages after the farmer has logged into the system
    if(session.getAttribute("farmuseremail") == null){
            out.println("<script type=\"text/javascript\">");
            out.println("alert('You need to login as a farmer first!!');");
            out.println("window.location.href = \"farmlogin.jsp\";");
            out.println("</script>");
    }
%>
<!--<meta http-equiv="refresh" content="0" />-->
<!--<META HTTP-EQUIV="Expires" CONTENT="1">-->
<!-- <meta http-equiv="cache-control" content="no-cache, must-revalidate, post-check=0, pre-check=0" />
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" /> -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<nav class="navbar navbar-inverse fixed-top">
  <div class="container-fixed">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="index.jsp">MkulimaOnline</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="farmshop.jsp">Shop</a></li> 
        <li><a href="farmingshop.jsp">Farming Shop</a></li> 
        <li><a href="postprod.jsp">Post product</a></li>
        <li><a href="myproducts.jsp">My products</a></li>
        <li><a href="pricing.jsp">Pricing</a></li>
        <li><a href="farmingsystems.jsp">Farming Systems</a></li>
        <li><a href="pestanddisease.jsp">Pests/Diseases</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span><%=session.getAttribute("farmuseremail")%></a></li>
        <li><a href="farmcheckout.jsp"><span class="glyphicon glyphicon-shopping-cart"></span></a>
        <li>
            <a href="#">
                <form action="LogOut">
                    <button type="submit" class="btn btn-primary">Logout <span class="glyphicon glyphicon-log-out"></span></button>
                </form>
            </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<style>
    nav{
        position: absolute;
    }
    .btn{
        background-color: black;
    }
    .btn:hover{
        background-color: black;
    }
</style>