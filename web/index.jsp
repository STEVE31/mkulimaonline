<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- Helps display the home page by placing it within the index file which is the initial page when the program loads-->
<html>
    <head>
        <%@ include file="beginheader.jsp" %>
        <title>Home Page</title>
    </head>
    <body>
        <jsp:include page="home.jsp"></jsp:include>    
        <%@ include file="footer.jsp" %>
    </body>
</html>
